module gitlab.com/Misakey/consent-backend

require (
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/google/uuid v1.1.1
	github.com/kat-co/vala v0.0.0-20170210184112-42e1d8b61f12 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/lib/pq v1.2.0
	github.com/pkg/errors v0.8.1
	github.com/pressly/goose v2.7.0-rc3+incompatible
	github.com/rs/zerolog v1.15.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.4.0
	github.com/volatiletech/inflect v0.0.0-20170731032912-e7201282ae8d // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.4.0+incompatible
	gitlab.com/Misakey/msk-sdk-go v0.0.0-20191025072618-05128997d0fb
)
