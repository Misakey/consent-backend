user-consents:
  get:
    summary: List user consents.
    description: List user consents with user_id and application_ids query params.
    tags:
      - User Consent
    parameters:
      - in: query
        name: user_id
        description: id of a user
        schema:
          type: string
          format: uuid
          example: "956ba5bb-f6c8-449e-b90a-21b86e29a233"
      - in: query
        name: application_ids
        description: ids of applications (comma separated list)
        schema:
          type: string
          format: uuid
          example: "956ba5bb-f6c8-449e-b90a-21b86e29a233,956ba5bb-f6c8-449e-b90a-21b86e29a233"
    responses:
      '200':
        description: status OK
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/UserConsent'
      '400':
        $ref: "common_responses.yaml#/components/responses/400BadRequest"
      '401':
        $ref: 'common_responses.yaml#/components/responses/401Unauthorized'
      '403':
        $ref: "common_responses.yaml#/components/responses/403Forbidden"

user-consent:
  patch:
    summary: Revoke a user consent.
    description: Revoke a user consent.
    tags:
      - User Consent
    parameters:
      - in: path
        name: id
        description: id of the User Consent
        schema:
          type: integer
          example: 1
        required: true
    responses:
      '204':
        $ref: "common_responses.yaml#/components/responses/204NoContent"
      '401':
        $ref: "common_responses.yaml#/components/responses/401Unauthorized"
      '403':
        $ref: "common_responses.yaml#/components/responses/403Forbidden"
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"

components:
  schemas:
    UserConsent:
      title: User Consent
      description: User Consent is an object representing a consent to share data between a user and an application purpose
      properties:
        id:
          description: the resource id
          type: integer
          example: 1
        user_id:
          description: the user id
          type: string
          example: "956ba5bb-f6c8-449e-b90a-21b86e29a233"
          format: uuid
        created_at:
          readOnly: true
          description: creation date of the resource
          type: string
          format: date-time
          example: "2017-07-21T17:32:28Z"
        application_purpose_id:
          description: id of application purpose
          type: integer
          example: 1
        revokated_at:
          description: revocation date of the resource
          type: string
          format: date-time
          example: "2017-07-21T17:32:28Z"
