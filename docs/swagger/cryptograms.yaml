cryptograms:
  post:
    summary: Create a new cryptogram.
    description: Create a cryptogram linked to a data channel and a producer application id.
    tags:
    - Cryptograms
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: array
            items:
              $ref: "#/components/schemas/Cryptogram"
    responses:
      '201':
        description: status created
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: "#/components/schemas/Cryptogram"
      '400':
        $ref: "common_responses.yaml#/components/responses/400BadRequest"
      '401':
        $ref: "common_responses.yaml#/components/responses/401Unauthorized"
      '403':
        $ref: "common_responses.yaml#/components/responses/403Forbidden"

  get:
    summary: List available cryptograms.
    description: List all accessible cryptograms for given owner, datatypes and a time frame (sorted by data_timestamp & id first)
    tags:
      - Cryptograms
    parameters:
      - in: query
        name: datatype_labels
        description: comma-separated list of datatype labels to filter on.
        schema:
          type: string
          enum: [unclassified, unclassified_purchase]
      - in: query
        name: purpose_labels
        required: true
        description: comma-separated list of purpose labels declaring the usage of the data.
        schema:
          type: string
          enum: [minimum_required,service_personalization,security_improvement,user_profiling,marketing_communication,provision_to_partners,data_science]
      - in: query
        required: true
        name: owner_user_id
        description: id of the owner user of cryptograms.
        schema:
          type: string
          format: uuid
          example: 18b88d48-ad48-43b9-a323-eab1de68b280
      - in: query
        name: producer_application_id
        description: id of producer applications (only retrievable by data owners)
        schema:
          type: string
          format: uuid
          example: 18b88d48-ad48-43b9-a323-eab1de68b280
      - in: query
        name: from_datetime
        description: minimum date corresponding to data sample timestamp.
        schema:
          type: string
          format: date-time
          example: "2019-05-28T13:04:44.582416Z"
      - in: query
        name: to_datetime
        description: maximum date corresponding to data sample timestamp.
        schema:
          type: string
          format: date-time
          example: "2019-05-28T13:04:44.582416Z"
      - in: query
        name: offset
        schema:
          type: integer
          example: 0
        description: cursor to couple with limit parameter to perform pagination
      - in: query
        required: true
        name: limit
        schema:
          type: integer
          maximum: 500
          default: 30
        description: maximum number of entities to list
    responses:
      '200':
        description: status OK
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: "#/components/schemas/Cryptogram"
      '400':
        $ref: "common_responses.yaml#/components/responses/400BadRequest"
      '401':
        $ref: "common_responses.yaml#/components/responses/401Unauthorized"
      '403':
        $ref: "common_responses.yaml#/components/responses/403Forbidden"
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"

components:
  schemas:
    Cryptogram:
      type: object
      title: A cryptogram entity contains information related to uploaded encrypted data.
      required:
        - data_channel_id
        - producer_application_id
        - content
        - data_timestamp
        - data_source
      properties:
        id:
          description: id of the cryptogram
          type: string
          format: uuid
          example: 7cbb1e6d-2457-488b-a673-95c4c13403280
          readOnly: true
        data_source:
          description: describes by which process this cryptogram was generated and uploaded
          type: string
          enum: ['takeout_google_pipeline_v_1']
        data_channel_id:
          description: id of the channel where the cryptogram has been uploaded
          type: string
          format: uuid
          example: 7cbb1e6d-2457-488b-a673-95c4c13403280
        producer_application_id:
          description: id of the producer application which uploaded the cryptogram
          type: string
          format: uuid
          example: 7cbb1e6d-2457-488b-a673-95c4c13403280
        content:
          description: the base64 encoded string of the content of the cryptogram
          type: string
          format: byte
          example: Vm91cyBzYXZleiwgbW9pIGplIG5lIGNyb2lzIHBhcyBxdSdpbCB5IGFpdCBkZSBib25uZSBvdSBkZSBtYXV2YWlzZSBzaXR1YXRpb24uIE1vaSwgc2kgamUgZGV2YWlzIHJlc3VtZXIgbWEgdmllIGF1am91cmQnaHVpIGF2ZWMgdm91cywgamUgZGlyYWlzIHF1ZSBjJ2VzdCBkJ2Fib3JkIGRlcyByZW5jb250cmVzLiBEZXMgZ2VucyBxdWkgbSdvbnQgdGVuZHUgbGEgbWFpbiwgcGV1dC1ldHJlIGEgdW4gbW9tZW50IG91IGplIG5lIHBvdXZhaXMgcGFzLCBvdSBqJ2V0YWlzIHNldWwgY2hleiBtb2kuIEV0IGMnZXN0IGFzc2V6IGN1cmlldXggZGUgc2UgZGlyZSBxdWUgbGVzIGhhc2FyZHMsIGxlcyByZW5jb250cmVzIGZvcmdlbnQgdW5lIGRlc3RpbmVlLi4uIFBhcmNlIHF1ZSBxdWFuZCBvbiBhIGxlIGdvdXQgZGUgbGEgY2hvc2UsIHF1YW5kIG9uIGEgbGUgZ291dCBkZSBsYSBjaG9zZSBiaWVuIGZhaXRlLCBsZSBiZWF1IGdlc3RlLCBwYXJmb2lzIG9uIG5lIHRyb3V2ZSBwYXMgbCdpbnRlcmxvY3V0ZXVyIGVuIGZhY2UgamUgZGlyYWlzLCBsZSBtaXJvaXIgcXVpIHZvdXMgYWlkZSBhIGF2YW5jZXIuIEFsb3JzIGNhIG4nZXN0IHBhcyBtb24gY2FzLCBjb21tZSBqZSBkaXNhaXMgbGEsIHB1aXNxdWUgbW9pIGF1IGNvbnRyYWlyZSwgaidhaSBwdSA6IGV0IGplIGRpcyBtZXJjaSBhIGxhIHZpZSwgamUgbHVpIGRpcyBtZXJjaSwgamUgY2hhbnRlIGxhIHZpZSwgamUgZGFuc2UgbGEgdmllLi4uIGplIG5lIHN1aXMgcXUnYW1vdXIgISBFdCBmaW5hbGVtZW50LCBxdWFuZCBiZWF1Y291cCBkZSBnZW5zIGF1am91cmQnaHVpIG1lIGRpc2VudCA8PCBNYWlzIGNvbW1lbnQgZmFpcy10dSBwb3VyIGF2b2lyIGNldHRlIGh1bWFuaXRlID8gPj4sIGV0IGJpZW4gamUgbGV1ciByZXBvbmRzIHRyZXMgc2ltcGxlbWVudCwgamUgbGV1ciBkaXMgcXVlIGMnZXN0IGNlIGdvdXQgZGUgbCdhbW91ciBjZSBnb3V0IGRvbmMgcXVpIG0nYSBwb3Vzc2UgYXVqb3VyZCdodWkgYSBlbnRyZXByZW5kcmUgdW5lIGNvbnN0cnVjdGlvbiBtZWNhbmlxdWUsIG1haXMgZGVtYWluIHF1aSBzYWl0ID8gUGV1dC1ldHJlIHNpbXBsZW1lbnQgYSBtZSBtZXR0cmUgYXUgc2VydmljZSBkZSBsYSBjb21tdW5hdXRlLCBhIGZhaXJlIGxlIGRvbiwgbGUgZG9uIGRlIHNvaS4uLiA=
        data_timestamp:
          description: date corresponding to the data sample contained within the cryptogram (RFC3339)
          type: string
          format: date-time
          example: "2012-11-01T22:08:41+00:00"
