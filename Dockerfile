FROM golang:1.12 as builder

WORKDIR /go/src/app
COPY . .
RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
    -mod=vendor \
    -installsuffix 'static' \
    -o /bin/consent-backend .

ENTRYPOINT ["/bin/consent-backend"]

FROM alpine:3.9

RUN until apk --update add ca-certificates; do sleep 1; done
COPY --from=builder /bin/consent-backend /bin/consent-backend
# add configuration file as configuration dev file to be integrated directly in our images
# in production, the file must be served via a mounted volume
COPY ./config/consent-config.toml /etc/consent-config.dev.toml
# set the version env variable used in the /version endpoint
ARG VERSION=unset
ENV VERSION $VERSION

EXPOSE 5000

ENTRYPOINT ["/bin/consent-backend"]
