package repo

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

type DataChannelSQLBoiler struct {
	db *sql.DB
}

func NewDataChannelSQLBoiler(db *sql.DB) *DataChannelSQLBoiler {
	return &DataChannelSQLBoiler{
		db: db,
	}
}

func (repo *DataChannelSQLBoiler) Insert(ctx context.Context, dataChannels []*model.DataChannel) error {
	tx, err := repo.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	for _, dataChannel := range dataChannels {
		err = dataChannel.Insert(ctx, tx, boil.Infer())
		if err != nil {
			rollbackError := tx.Rollback()
			if rollbackError != nil {
				logger.FromCtx(ctx).Warn().Err(rollbackError).Msg("rollback error")
			}
			// try to consider error cause as pq error to understand deeper the error
			pqErr, ok := merror.Cause(err).(*pq.Error)
			if !ok { // if not, return error directly
				return err
			}
			if ok && pqErr.Code.Name() == "unique_violation" && pqErr.Constraint == "data_channel_public_key_key" {
				return merror.Conflict().Describe(err.Error()).Detail("public_key", merror.DVConflict)
			}
			if pqErr.Code.Name() == "unique_violation" && pqErr.Constraint == "data_channel_owner_user_id_datatype_label_key" {
				return merror.Conflict().Describe(err.Error()).
					Detail("datatype_label", merror.DVConflict).
					Detail("user_id", merror.DVConflict)
			}
			if pqErr.Code.Name() == "invalid_text_representation" {
				return merror.BadRequest().Describe(err.Error()).Detail("datatype_label", merror.DVInvalid)
			}

			return err
		}
	}
	return tx.Commit()
}

func (repo *DataChannelSQLBoiler) List(ctx context.Context, filters model.DataChannelFilters) ([]*model.DataChannel, error) {
	mods := []qm.QueryMod{}

	if filters.OwnerUserID != "" {
		mods = append(mods, qm.Where("owner_user_id=?", filters.OwnerUserID))
	}
	if len(filters.DatatypeLabels) > 0 {
		labels := slices.StringSliceToInterfaceSlice(filters.DatatypeLabels)
		mods = append(mods, qm.WhereIn("datatype_label IN ?", labels...))
	}

	retrievedDataChannels, err := model.DataChannels(mods...).All(ctx, repo.db)

	if retrievedDataChannels == nil {
		retrievedDataChannels = []*model.DataChannel{}
	}
	return retrievedDataChannels, err
}

func (repo *DataChannelSQLBoiler) Get(ctx context.Context, id int) (*model.DataChannel, error) {
	datChannel, err := model.FindDataChannel(ctx, repo.db, id)
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("id", merror.DVNotFound)
	}
	return datChannel, err
}
