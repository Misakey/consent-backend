package repo

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/Misakey/msk-sdk-go/rester"

	"gitlab.com/Misakey/consent-backend/src/model"
)

// HTTP implements Hydra repository interface using HTTP REST
type SSOConsentHTTP struct {
	rester rester.Client
}

func NewSSOConsentHTTP(rester rester.Client) *SSOConsentHTTP {
	return &SSOConsentHTTP{
		rester: rester,
	}
}

// ConsentInfo retrieves information about a consent requwest
func (h *SSOConsentHTTP) GetInfo(ctx context.Context, challenge string) (*model.ConsentInfo, error) {
	consInfo := model.ConsentInfo{}
	params := url.Values{}
	params.Add("challenge", challenge)
	route := fmt.Sprintf("/oauth2/auth/requests/consent?%s", params.Encode())
	err := h.rester.Get(ctx, route, nil, &consInfo)
	if err != nil {
		return nil, err
	}
	return &consInfo, nil
}

// ConsentAccept confirms to Hydra consent has been approved by a user
func (h *SSOConsentHTTP) Accept(ctx context.Context, challenge string, accReq model.ConsentAcceptRequest) (model.ConsentAcceptInfo, error) {
	accInfo := model.ConsentAcceptInfo{}
	params := url.Values{}
	params.Add("challenge", challenge)
	route := fmt.Sprintf("/oauth2/auth/requests/consent/accept?%s", params.Encode())

	err := h.rester.Put(ctx, route, nil, accReq, &accInfo)
	if err != nil {
		return accInfo, err
	}
	return accInfo, nil
}

// ConsentReject informs Hydra user rejected a consent request.
func (h *SSOConsentHTTP) Reject(ctx context.Context, challenge string, rejectReq model.RejectRequest) error {
	params := url.Values{}
	params.Add("challenge", challenge)
	route := fmt.Sprintf("/oauth2/auth/requests/consent/reject?%s", params.Encode())
	err := h.rester.Put(ctx, route, nil, rejectReq, nil)
	if err != nil {
		return err
	}
	return nil
}

// ConsentList : Lists all consent sessions of a subject
func (h *SSOConsentHTTP) List(ctx context.Context, id string) ([]model.ConsentInfoClient, error) {
	consentsList := []model.ConsentInfoClient{}
	route := fmt.Sprintf("/oauth2/auth/sessions/consent?subject=%s", id)

	err := h.rester.Get(ctx, route, nil, &consentsList)
	if err != nil {
		return nil, err
	}
	return consentsList, nil
}
