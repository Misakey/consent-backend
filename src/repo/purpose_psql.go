package repo

import (
	"context"
	"database/sql"

	"github.com/volatiletech/sqlboiler/queries/qm"

	"gitlab.com/Misakey/consent-backend/src/model"
)

// PurposePSQL used for purpose storage using sqlboiler ORM
type PurposePSQL struct {
	db *sql.DB
}

// PurposePSQL constructor
func NewPurposePSQL(db *sql.DB) *PurposePSQL {
	return &PurposePSQL{
		db: db,
	}
}

// List
func (pp *PurposePSQL) List(ctx context.Context) ([]*model.Purpose, error) {
	mods := []qm.QueryMod{}

	purposes, err := model.Purposes(mods...).All(ctx, pp.db)
	if err != nil {
		return nil, err
	}

	// Return an empty array if no result
	if purposes == nil {
		purposes = []*model.Purpose{}
	}
	return purposes, err
}
