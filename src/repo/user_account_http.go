package repo

import (
	"context"
	"fmt"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"

	"gitlab.com/Misakey/consent-backend/src/adaptor"
	"gitlab.com/Misakey/consent-backend/src/model"
)

// UserAccountHTTP implements User repository interface using HTTP REST
type UserAccountHTTP struct {
	rester adaptor.Rester
	token  string
}

// NewUserAccountPSQL is UserAccountPSQL constructor
func NewUserAccountHTTP(rester adaptor.Rester, token string) *UserAccountHTTP {
	return &UserAccountHTTP{
		rester: rester,
		token:  token,
	}
}

func (uah *UserAccountHTTP) Get(ctx context.Context, id string) (*model.UserAccount, error) {
	user := model.UserAccount{}
	route := fmt.Sprintf("/users/%s", id)

	// Set Service JWT in context
	ctx = ajwt.SetAccessClaimsJWT(ctx, uah.token)

	err := uah.rester.Get(ctx, route, nil, &user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}
