package repo

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/volatiletech/null"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"github.com/volatiletech/sqlboiler/queries/qmhelper"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

// UserConsentPSQL used for user consent storage using sqlboiler ORM
type UserConsentPSQL struct {
	db *sql.DB
}

// UserConsentPSQL constructor
func NewUserConsentPSQL(db *sql.DB) *UserConsentPSQL {
	return &UserConsentPSQL{
		db: db,
	}
}

// Insert
func (uc *UserConsentPSQL) Insert(ctx context.Context, userConsent model.UserConsent) error {
	err := userConsent.Insert(ctx, uc.db, boil.Infer())
	if err != nil {
		return err
	}

	return err
}

// Get
func (uc *UserConsentPSQL) Get(ctx context.Context, id int) (*model.UserConsent, error) {
	userConsent, err := model.FindUserConsent(ctx, uc.db, id)
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("id", merror.DVNotFound)
	}
	return userConsent, err
}

// List
func (uc *UserConsentPSQL) List(ctx context.Context, filters model.UserConsentFilters) ([]*model.UserConsent, error) {
	mods := []qm.QueryMod{}

	if filters.UserID != "" {
		mods = append(mods, qm.Where("user_id=?", filters.UserID))
	}

	if len(filters.ApplicationPurposeIDs) > 0 {
		appPurposeIDs := slices.IntSliceToInterfaceSlice(filters.ApplicationPurposeIDs)
		mods = append(mods, qm.WhereIn("application_purpose_id IN ?", appPurposeIDs...))
	}

	if filters.UniqueApplicationPurpose.Valid && filters.UniqueApplicationPurpose.Bool {
		mods = append(mods, qm.Select("distinct application_purpose_id"))
	}

	if len(filters.ApplicationIDs) > 0 {
		joinBase := "application_purpose ON application_purpose.id = user_consent.application_purpose_id"
		joinQuery := fmt.Sprintf("%s AND application_id IN (%s)", joinBase, slices.EscapedStrings(filters.ApplicationIDs))
		mods = append(mods, qm.InnerJoin(joinQuery))
	}

	// force remove to delete entities
	mods = append(mods, qm.Where("to_delete=?", false))
	// revoked filter is always used - we use null.String to simulate a nil value
	mods = append(mods, qmhelper.WhereNullEQ("revoked_at", filters.Revoked, null.NewString("", false)))
	userConsents, err := model.UserConsents(mods...).All(ctx, uc.db)
	if err != nil {
		return nil, err
	}
	// Return an empty array if no result
	if userConsents == nil {
		return []*model.UserConsent{}, nil
	}
	return userConsents, err
}

// PartialUpdate (Patch)
func (uc *UserConsentPSQL) PartialUpdate(ctx context.Context, userConsent *model.UserConsent, fields ...string) error {
	rowsAff, err := userConsent.Update(ctx, uc.db, boil.Whitelist(fields...))
	if err != nil {
		return err
	}
	if rowsAff == 0 {
		return merror.NotFound().Describe("no rows affected")
	}
	return nil
}

// Delete updates the UserConsent entity to set to_delete to true.
func (c *UserConsentPSQL) Delete(ctx context.Context, userConsent *model.UserConsent) error {
	userConsent.ToDelete = null.BoolFrom(true)

	rowsAff, err := userConsent.Update(ctx, c.db, boil.Infer())
	if err != nil {
		return err
	}
	if rowsAff == 0 {
		return merror.NotFound().Describe("no rows affected")
	}
	return nil
}
