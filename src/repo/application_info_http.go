package repo

import (
	"context"
	"fmt"

	"gitlab.com/Misakey/consent-backend/src/adaptor"
	"gitlab.com/Misakey/consent-backend/src/model"
)

// HTTP implements ApplicationInfo's repository interface using HTTP REST
type ApplicationInfoHTTP struct {
	rester adaptor.Rester
}

// NewApplicationInfoHTTP is HTTP ApplicationInfo's structure constructor
func NewApplicationInfoHTTP(rester adaptor.Rester) *ApplicationInfoHTTP {
	return &ApplicationInfoHTTP{
		rester: rester,
	}
}

// Get retrieve an ApplicationInfo from application-backend using an application id.
func (h *ApplicationInfoHTTP) Get(ctx context.Context, id string) (*model.ApplicationInfo, error) {
	route := fmt.Sprintf("/application-info/%s", id)
	appInfo := model.ApplicationInfo{}

	err := h.rester.Get(ctx, route, nil, &appInfo)

	return &appInfo, err
}
