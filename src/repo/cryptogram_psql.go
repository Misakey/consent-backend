package repo

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

type CryptogramSQLBoiler struct {
	db *sql.DB
}

// NewCryptogramSQLBoiler constructor
func NewCryptogramSQLBoiler(db *sql.DB) *CryptogramSQLBoiler {
	return &CryptogramSQLBoiler{
		db: db,
	}
}

// Insert
func (repo *CryptogramSQLBoiler) Insert(ctx context.Context, cryptogram *model.Cryptogram) error {
	err := cryptogram.Insert(ctx, repo.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		// TODO: add ToLargeEntity from msk
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if ok && pqErr.Code.Name() == "string_data_right_truncation" {
			return merror.Conflict().
				Describe(err.Error()).
				From(merror.OriQuery).
				Detail("content", merror.DVInvalid)
		}
	}
	return err
}

// List
func (repo *CryptogramSQLBoiler) List(ctx context.Context, filters model.CryptogramlFilters) ([]*model.Cryptogram, error) {
	mods := []qm.QueryMod{}

	// sort by most recent first
	mods = append(mods, qm.OrderBy("data_timestamp DESC, id DESC"))

	if filters.OwnerUserID != "" {
		mods = append(mods, qm.InnerJoin("data_channel ON data_channel.id = cryptogram.data_channel_id AND data_channel.owner_user_id = ?", filters.OwnerUserID))
	}
	if len(filters.DatatypeLabels) > 0 {
		datatypeLabels := slices.StringSliceToInterfaceSlice(filters.DatatypeLabels)
		mods = append(mods, qm.WhereIn("data_channel.datatype_label IN ?", datatypeLabels...))
	}
	if filters.ProducerApplicationID != "" {
		mods = append(mods, qm.Where("producer_application_id = ?", filters.ProducerApplicationID))
	}
	if !filters.FromDatetime.IsZero() {
		mods = append(mods, qm.Where("data_timestamp >= ?", filters.FromDatetime))
	}
	if !filters.ToDatetime.IsZero() {
		mods = append(mods, qm.Where("data_timestamp <= ?", filters.ToDatetime))
	}
	if filters.Offset != nil {
		mods = append(mods, qm.Offset(int(*filters.Offset)))
	}
	if filters.Limit != nil {
		mods = append(mods, qm.Limit(int(*filters.Limit)))
	}

	cryptograms, err := model.Cryptograms(mods...).All(ctx, repo.db)
	if cryptograms == nil {
		cryptograms = []*model.Cryptogram{}
	}
	return cryptograms, err
}
