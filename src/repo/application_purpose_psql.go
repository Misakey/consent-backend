package repo

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

// ApplicationPurposePSQL used for application purpose storage using sqlboiler ORM
type ApplicationPurposePSQL struct {
	db *sql.DB
}

// ApplicationPurposePSQL constructor
func NewApplicationPurposePSQL(db *sql.DB) *ApplicationPurposePSQL {
	return &ApplicationPurposePSQL{
		db: db,
	}
}

// Insert
func (ap *ApplicationPurposePSQL) Insert(ctx context.Context, appPurpose *model.ApplicationPurpose) error {
	err := appPurpose.Insert(ctx, ap.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if ok && pqErr.Code.Name() == "unique_violation" && pqErr.Constraint == "application_purpose_purpose_label_application_id_key" {
			return merror.Conflict().Describe(err.Error()).
				Detail("purpose_label", merror.DVConflict).
				Detail("application_id", merror.DVConflict)
		}
	}
	return err
}

// Get
func (ap *ApplicationPurposePSQL) Get(ctx context.Context, id int) (*model.ApplicationPurpose, error) {
	appPurposes, err := model.FindApplicationPurpose(ctx, ap.db, id)
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("id", merror.DVNotFound)
	}
	return appPurposes, err
}

// List
func (ap *ApplicationPurposePSQL) List(ctx context.Context, filters model.ApplicationPurposeFilters) ([]*model.ApplicationPurpose, error) {
	mods := []qm.QueryMod{}

	if len(filters.IDs) > 0 {
		ids := slices.IntSliceToInterfaceSlice(filters.IDs)
		mods = append(mods, qm.WhereIn("id IN ?", ids...))
	}

	if len(filters.AppIDs) > 0 {
		appIDs := slices.StringSliceToInterfaceSlice(filters.AppIDs)
		mods = append(mods, qm.WhereIn("application_id IN ?", appIDs...))
	}

	if len(filters.PurposeLabels) > 0 {
		purposeLabels := slices.StringSliceToInterfaceSlice(filters.PurposeLabels)
		mods = append(mods, qm.WhereIn("purpose_label IN ?", purposeLabels...))
	}

	appPurposes, err := model.ApplicationPurposes(mods...).All(ctx, ap.db)
	if err != nil {
		return nil, err
	}

	// Return an empty array if no result
	if appPurposes == nil {
		appPurposes = []*model.ApplicationPurpose{}
	}
	return appPurposes, err
}

// Delete
func (ap *ApplicationPurposePSQL) Delete(ctx context.Context, appPurpose *model.ApplicationPurpose) error {
	_, err := appPurpose.Delete(ctx, ap.db)
	if err != nil {
		return err
	}
	return nil
}
