package repo

import (
	"context"
	"fmt"

	"gitlab.com/Misakey/consent-backend/src/adaptor"
	"gitlab.com/Misakey/consent-backend/src/model"
)

// HTTP implements SSO client hydra repository interface using HTTP REST
type SSOClientHTTP struct {
	rester adaptor.Rester
}

// NewSSOClientHTTP is HTTP SSO client structure constructor
func NewSSOClientHTTP(rester adaptor.Rester) *SSOClientHTTP {
	return &SSOClientHTTP{
		rester: rester,
	}
}

// GetClient: retrieve a SSO client from hydra using a client id.
func (h *SSOClientHTTP) Get(ctx context.Context, id string) (*model.SSOClient, error) {
	cli := model.SSOClient{}
	route := fmt.Sprintf("/clients/%s", id)

	err := h.rester.Get(ctx, route, nil, &cli)
	if err != nil {
		return nil, err
	}
	return &cli, nil
}

// UpdateClient: update SSO client on Hydra
func (h *SSOClientHTTP) Update(ctx context.Context, ssoClient *model.SSOClient) error {
	route := fmt.Sprintf("/clients/%s", ssoClient.ID)

	return h.rester.Put(ctx, route, nil, ssoClient, ssoClient)
}
