package repo

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

// ApplicationPurposeDatatypePSQL used for application purpose datatype storage using sqlboiler ORM
type ApplicationPurposeDatatypePSQL struct {
	db *sql.DB
}

// ApplicationPurposeDatatypePSQL constructor
func NewApplicationPurposeDatatypePSQL(db *sql.DB) *ApplicationPurposeDatatypePSQL {
	return &ApplicationPurposeDatatypePSQL{
		db: db,
	}
}

// Insert
func (ap *ApplicationPurposeDatatypePSQL) Insert(ctx context.Context, appPurpose *model.ApplicationPurposeDatatype) error {
	err := appPurpose.Insert(ctx, ap.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if ok && pqErr.Code.Name() == "unique_violation" && pqErr.Constraint == "application_purpose_datatype_datatype_id_application_purpos_key" {
			return merror.Conflict().Describe(err.Error()).
				Detail("datatype_id", merror.DVConflict).
				Detail("application_purpose_id", merror.DVConflict)
		}
	}
	return err
}

// Get
func (ap *ApplicationPurposeDatatypePSQL) Get(ctx context.Context, id int) (*model.ApplicationPurposeDatatype, error) {
	appPurposeDatatype, err := model.FindApplicationPurposeDatatype(ctx, ap.db, id)
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("id", merror.DVNotFound)
	}
	return appPurposeDatatype, err
}

// List
func (ap *ApplicationPurposeDatatypePSQL) List(ctx context.Context, filters model.ApplicationPurposeDatatypeFilters) ([]*model.ApplicationPurposeDatatype, error) {
	mods := []qm.QueryMod{}

	if len(filters.IDs) > 0 {
		query := fmt.Sprintf("id IN (%s)", slices.EscapedStrings(filters.IDs))
		mods = append(mods, qm.WhereIn(query))
	}

	appPurposesDatatypes, err := model.ApplicationPurposeDatatypes(mods...).All(ctx, ap.db)
	if err != nil {
		return nil, err
	}

	// Return an empty array if no result
	if appPurposesDatatypes == nil {
		appPurposesDatatypes = []*model.ApplicationPurposeDatatype{}
	}
	return appPurposesDatatypes, err
}

// Delete
func (ap *ApplicationPurposeDatatypePSQL) Delete(ctx context.Context, appPurposeDatatype *model.ApplicationPurposeDatatype) error {
	_, err := appPurposeDatatype.Delete(ctx, ap.db)
	return err
}
