package cmd

import (
	"fmt"
	"os"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/Misakey/msk-sdk-go/bubble"
	"gitlab.com/Misakey/msk-sdk-go/config"
	mecho "gitlab.com/Misakey/msk-sdk-go/echo"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	mhttp "gitlab.com/Misakey/msk-sdk-go/rester/http"

	"gitlab.com/Misakey/consent-backend/src/adaptor/sql"
	"gitlab.com/Misakey/consent-backend/src/controller"
	"gitlab.com/Misakey/consent-backend/src/repo"
	"gitlab.com/Misakey/consent-backend/src/service"
)

var cfgFile string
var goose string
var env = os.Getenv("ENV")

func init() {
	cobra.OnInitialize()
	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")
	RootCmd.PersistentFlags().StringVar(&goose, "goose", "up", "goose command")
	RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

var RootCmd = &cobra.Command{
	Use:   "consent",
	Short: "Run the application consent service",
	Long:  `This service handles user consent`,
	Run: func(cmd *cobra.Command, args []string) {
		initService()
	},
}

func initService() {
	// init logger
	log.Logger = logger.ZerologLogger()

	// init error needles
	bubble.AddNeedle(bubble.PSQLNeedle{})
	bubble.AddNeedle(bubble.ValidatorNeedle{})
	bubble.AddNeedle(bubble.EchoNeedle{})
	bubble.Lock()

	initDefaultConfig()

	// init echo framework with compressed HTTP responses, custom logger format and custom validator
	e := echo.New()
	e.Use(mecho.NewZerologLogger())
	e.Use(mecho.NewLogger())
	e.Use(mecho.NewCORS())
	e.Use(middleware.Recover())

	e.Validator = mecho.NewValidator()
	e.HTTPErrorHandler = mecho.Error
	e.HideBanner = true

	//init auth middleware
	jwtValidator := mecho.NewJWTMidlw(true)

	// init db connections
	dbConn, err := sql.NewPostgreConn(os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal().Err(err)
	}

	// init rester
	hydraRester := mhttp.NewClient(viper.GetString("hydra.admin_url"), viper.GetBool("hydra.secure"))
	accountRester := mhttp.NewClient(viper.GetString("auth.endpoint"), viper.GetBool("auth.secure"))
	applicationRester := mhttp.NewClient(viper.GetString("application.endpoint"), viper.GetBool("application.secure"))

	// init repository
	ssoConsentRepo := repo.NewSSOConsentHTTP(hydraRester)
	authRester := repo.NewUserAccountHTTP(accountRester, viper.GetString("server.token"))
	applicationRepo := repo.NewApplicationInfoHTTP(applicationRester)
	ssoClientRepo := repo.NewSSOClientHTTP(hydraRester)

	purposeRepo := repo.NewPurposePSQL(dbConn)
	applicationPurposeRepo := repo.NewApplicationPurposePSQL(dbConn)
	applicationPurposeDatatypeRepo := repo.NewApplicationPurposeDatatypePSQL(dbConn)
	userConsentRepo := repo.NewUserConsentPSQL(dbConn)
	dataChannelRepo := repo.NewDataChannelSQLBoiler(dbConn)
	cryptogramRepo := repo.NewCryptogramSQLBoiler(dbConn)

	// init service
	purposeService := service.NewPurpose(purposeRepo)
	applicationPurposeService := service.NewApplicationPurpose(applicationPurposeRepo)
	applicationPurposeDatatypeService := service.NewApplicationPurposeDatatype(applicationPurposeDatatypeRepo, applicationPurposeRepo)
	userConsentService := service.NewUserConsent(userConsentRepo)
	consentService := service.NewConsent(ssoConsentRepo, userConsentRepo)
	consentManager := service.NewConsentManager(
		authRester, ssoConsentRepo, applicationPurposeRepo, userConsentRepo, ssoClientRepo,
		viper.GetString("consent.page_url"), viper.GetString("misadmin.client_id"),
	)
	dataChannelService := service.NewDataChannel(dataChannelRepo)
	cryptogramService := service.NewCryptogram(cryptogramRepo, applicationRepo, dataChannelRepo)

	// init controller
	consentHydraController := controller.NewConsentHydraEcho(consentManager)
	purposeController := controller.NewPurposeEcho(purposeService)
	applicationPurposeController := controller.NewApplicationPurposeEcho(applicationPurposeService)
	applicationPurposeDatatypeController := controller.NewApplicationPurposeDatatypeEcho(applicationPurposeDatatypeService)
	userConsentController := controller.NewUserConsentEcho(userConsentService)
	consentController := controller.NewConsentEcho(consentService)
	dataChannelController := controller.NewDataChannelEcho(dataChannelService)
	cryptogramController := controller.NewCryptogramEcho(cryptogramService)

	// Init generic
	genericController := controller.NewGeneric()

	// Bind purpose routes
	purpose := e.Group("purposes")
	purpose.GET("", purposeController.List)

	// Bind application-purpose routes
	applicationPurpose := e.Group("application-purposes")
	applicationPurpose.POST("", applicationPurposeController.Create, jwtValidator)
	applicationPurpose.GET("", applicationPurposeController.List)
	applicationPurpose.DELETE("/:id", applicationPurposeController.Delete, jwtValidator)

	// - consent flow
	consent := e.Group("consent")
	consent.GET("", consentHydraController.InitConsent)
	consent.POST("", consentHydraController.Consent)

	// Bind application-purpose-datatype routes
	applicationPurposeDatatype := e.Group("application-purpose-datatypes")
	applicationPurposeDatatype.POST("", applicationPurposeDatatypeController.Create, jwtValidator)
	applicationPurposeDatatype.GET("", applicationPurposeDatatypeController.List)
	applicationPurposeDatatype.DELETE("/:id", applicationPurposeDatatypeController.Delete, jwtValidator)

	// Bind generic routes
	generic := e.Group("")
	generic.GET("/version", genericController.Version)

	// Bind user-consent routes
	userConsent := e.Group("user-consents")
	userConsent.GET("", userConsentController.List, jwtValidator)
	userConsent.PATCH("/:id", userConsentController.PartialUpdate, jwtValidator)

	user := e.Group("users")
	user.DELETE("/:id", consentController.Delete, jwtValidator)

	// Bind data channel routes
	dataChannel := e.Group("data-channels")
	dataChannel.POST("", dataChannelController.Create, jwtValidator)
	dataChannel.GET("", dataChannelController.List, jwtValidator)

	// Bind cryptogram routes
	cryptogram := e.Group("cryptograms")
	cryptogram.POST("", cryptogramController.Create, jwtValidator)
	cryptogram.GET("", cryptogramController.List, jwtValidator)

	// launch echo server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", viper.GetInt("server.port"))))
}

func initDefaultConfig() {
	// always look for the configuration file in the /etc folder
	viper.AddConfigPath("/etc/")

	// set defaults for configuration
	viper.SetDefault("server.port", 5000)
	viper.SetDefault("hydra.secure", true)
	viper.SetDefault("auth.secure", true)

	// handle envs
	switch env {
	case "production":
		viper.SetConfigName("consent-config")
	case "development":
		viper.SetConfigName("consent-config.dev")
		log.Info().Msg("{} Development mode is activated. {}")
	default:
		log.Fatal().Msg("unknown ENV value (should be production|development)")
	}

	// try reading in a config
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Err(err).Msg("could not read configuration")
	}

	// handle missing mandatory fields
	mandatoryFields := []string{
		"consent.page_url", "auth.endpoint",
		"misadmin.client_id", "hydra.admin_url",
		"server.token",
	}

	config.FatalIfMissing(mandatoryFields)
	config.Print(nil)
}
