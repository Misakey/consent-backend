package model

import (
	"github.com/volatiletech/null"
)

// UserConsent filter
type UserConsentFilters struct {
	UserID                   string `validate:"required,uuid"`
	ApplicationPurposeIDs    []int
	ApplicationIDs           []string `validate:"dive,uuid"`
	Revoked                  bool
	UniqueApplicationPurpose null.Bool
}
