package model

// UserDataConsents represents what data user consents to share
type UserDataConsents struct {
	Challenge             string `json:"challenge" validate:"required"`
	ApplicationPurposeIDs []int  `json:"application_purpose_ids" validate:"required"`
}

// ConsentAcceptInfo contains data about what to do after consent success (from Hydra)
type ConsentAcceptInfo struct {
	RedirectTo string `json:"redirect_to"`
}

type ConsentInfo struct {
	Skip           bool          `json:"skip"`
	RequestedScope []string      `json:"requested_scope"`
	Info           SSOClientInfo `json:"client"`
	Subject        string        `json:"subject"`
}

type ConsentInfoClient struct {
	ConsentRequest SSOClientRequestInfo `json:"consent_request"`
}

type IDToken struct {
}

type Session struct {
	IDToken struct {
		Scope string `json:"sco"`
	} `json:"id_token"`
}

// ConsentRejectRequest contains data about user consent acceptance (to Hydra)
type ConsentAcceptRequest struct {
	GrantScope  []string `json:"grant_scope"`
	Remember    bool     `json:"remember"`
	RememberFor int      `json:"remember_for"`
	Session     Session  `json:"session"`
}

// RejectRequest contains data about user login/consent rejection (to Hydra)
type RejectRequest struct {
	Error            string `json:"error"`
	ErrorDebug       string `json:"error_debug"`
	ErrorDescription string `json:"error_description"`
	ErrorHint        string `json:"error_hint"`
	StatusCode       int    `json:"status_code"`
}
