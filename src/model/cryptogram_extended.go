package model

import "time"

// Filters for Cryptogram listing
type CryptogramlFilters struct {
	DatatypeLabels        []string `validate:"omitempty,dive,oneof=unclassified unclassified_purchase"`
	PurposeLabels         []string `validate:"required,dive,oneof=minimum_required service_personalization security_improvement user_profiling marketing_communication provision_to_partners data_science"`
	OwnerUserID           string   `validate:"required,uuid"`
	ProducerApplicationID string   `validate:"omitempty,uuid"`
	FromDatetime          time.Time
	ToDatetime            time.Time
	Limit                 *int64 `validate:"required,max=500"`
	Offset                *int64
}
