// Code generated by SQLBoiler 3.5.0 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package model

import (
	"strconv"

	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/strmangle"
)

// M type is for providing columns and column values to UpdateAll.
type M map[string]interface{}

// ErrSyncFail occurs during insert when the record could not be retrieved in
// order to populate default value information. This usually happens when LastInsertId
// fails or there was a primary key configuration that was not resolvable.
var ErrSyncFail = errors.New("model: failed to synchronize data after insert")

type insertCache struct {
	query        string
	retQuery     string
	valueMapping []uint64
	retMapping   []uint64
}

type updateCache struct {
	query        string
	valueMapping []uint64
}

func makeCacheKey(cols boil.Columns, nzDefaults []string) string {
	buf := strmangle.GetBuffer()

	buf.WriteString(strconv.Itoa(cols.Kind))
	for _, w := range cols.Cols {
		buf.WriteString(w)
	}

	if len(nzDefaults) != 0 {
		buf.WriteByte('.')
	}
	for _, nz := range nzDefaults {
		buf.WriteString(nz)
	}

	str := buf.String()
	strmangle.PutBuffer(buf)
	return str
}

// Enum values for cryptogram_data_source_enum
const (
	CryptogramDataSourceEnumTakeoutGooglePipelineV1 = "takeout_google_pipeline_v_1"
)

// Enum values for purpose_label_enum
const (
	PurposeLabelEnumMinimumRequired        = "minimum_required"
	PurposeLabelEnumServicePersonalization = "service_personalization"
	PurposeLabelEnumSecurityImprovement    = "security_improvement"
	PurposeLabelEnumUserProfiling          = "user_profiling"
	PurposeLabelEnumMarketingCommunication = "marketing_communication"
	PurposeLabelEnumProvisionToPartners    = "provision_to_partners"
	PurposeLabelEnumDataScience            = "data_science"
)

// Enum values for datatype_label_enum
const (
	DatatypeLabelEnumUnclassified         = "unclassified"
	DatatypeLabelEnumUnclassifiedPurchase = "unclassified_purchase"
)
