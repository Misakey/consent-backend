package model

type SSOClient struct {
	ID string `json:"client_id"`

	Name    string `json:"client_name"`
	LogoURI string `json:"logo_uri"`

	Scope         string   `json:"scope"`
	GrantTypes    []string `json:"grant_types"`
	RedirectURIs  []string `json:"redirect_uris" validate:"required"`
	ResponseTypes []string `json:"response_types"`

	Audience           []string `json:"audience"`
	AllowedCorsOrigins []string `json:"allowed_cors_origins" validate:"required"`

	SubjectType               string `json:"subject_type"`
	UserinfoSignedResponseALG string `json:"userinfo_signed_response_ald"`
	TokenEndpointAuthMethod   string `json:"token_endpoint_auth_method"`
	Secret                    string `json:"client_secret"`
	SecretExpiresAt           int    `json:"client_secret_expires_at"`
}

type SSOClientRequestInfo struct {
	Client SSOClientInfo `json:"client"`
}

type SSOClientInfo struct {
	AppName  string `json:"client_name"`
	ClientID string `json:"client_id"`
	LogoURI  string `json:"logo_uri"`
}
