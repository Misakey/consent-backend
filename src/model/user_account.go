package model

import (
	"github.com/volatiletech/null"
)

// UserAccount is an object representing a user
type UserAccount struct {
	ID          string      `json:"id"`
	Email       string      `json:"email"`
	DisplayName string      `json:"display_name"`
	AvatarURI   null.String `json:"avatar_uri"`
}
