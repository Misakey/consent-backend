package model

// Filters for ApplicationPurpose listing
type ApplicationPurposeFilters struct {
	IDs           []int    `validate:"required_without=AppIDs PurposeLabels"`
	AppIDs        []string `validate:"required_without=IDs PurposeLabels"`
	PurposeLabels []string `validate:"required_without=IDs AppIDs,omitempty,dive,oneof=minimum_required service_personalization security_improvement user_profiling marketing_communication provision_to_partners data_science"`
}
