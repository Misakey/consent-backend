package model

// ApplicationInfo contains all info about an application
type ApplicationInfo struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
