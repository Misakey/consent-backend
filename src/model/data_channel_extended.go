package model

// Filters for DataChannel listing
type DataChannelFilters struct {
	OwnerUserID    string   `validate:"required,uuid"`
	DatatypeLabels []string `validate:"omitempty,dive,oneof=unclassified unclassified_purchase"`
}
