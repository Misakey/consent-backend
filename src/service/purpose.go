package service

import (
	"context"

	"gitlab.com/Misakey/consent-backend/src/model"
)

type Purpose struct {
	repo purposeRepo
}

func NewPurpose(repo purposeRepo) *Purpose {
	return &Purpose{
		repo: repo,
	}
}

func (p Purpose) List(ctx context.Context) ([]*model.Purpose, error) {
	return p.repo.List(ctx)
}
