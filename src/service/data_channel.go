package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/model"
)

type DataChannel struct {
	repo dataChannelRepo
}

func NewDataChannel(repo dataChannelRepo,
) *DataChannel {
	return &DataChannel{
		repo: repo,
	}
}

func (srvc *DataChannel) Create(ctx context.Context, dataChannels []*model.DataChannel) error {
	accesses := ajwt.GetAccesses(ctx)
	if accesses == nil {
		return merror.Forbidden()
	}
	for _, dataChannel := range dataChannels {
		if accesses.IsNotUser(dataChannel.OwnerUserID) {
			return merror.Forbidden().
				Detail("owner_user_id", merror.DVForbidden).
				Describef("cannot create data channel for %s", dataChannel.OwnerUserID)
		}
	}

	return srvc.repo.Insert(ctx, dataChannels)
}

func (srvc *DataChannel) List(ctx context.Context, filters model.DataChannelFilters) ([]*model.DataChannel, error) {
	accesses := ajwt.GetAccesses(ctx)
	if accesses == nil {
		return nil, merror.Forbidden()
	}
	// TODO when there are data consumers other than owner:
	// allow conusmer if owner consented to it
	if accesses.IsNotUser(filters.OwnerUserID) {
		return nil, merror.Forbidden().
			Detail("owner_user_id", merror.DVForbidden).
			Describef("cannot list data channels owner by %s", filters.OwnerUserID)
	}
	return srvc.repo.List(ctx, filters)
}
