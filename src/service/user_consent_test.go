package service

import (
	"context"
	"testing"
	"time"

	"gitlab.com/Misakey/consent-backend/src/model"

	"github.com/stretchr/testify/assert"
	"github.com/volatiletech/null"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"
)

type userConsentMock struct {
	get           func(ctx context.Context, id int) (*model.UserConsent, error)
	list          func(ctx context.Context, filters model.UserConsentFilters) ([]*model.UserConsent, error)
	partialUpdate func(ctx context.Context, uc *model.UserConsent, fields ...string) error
}

func (m userConsentMock) List(ctx context.Context, filters model.UserConsentFilters) ([]*model.UserConsent, error) {
	return m.list(ctx, filters)
}

func (m userConsentMock) PartialUpdate(ctx context.Context, userConsent *model.UserConsent, fields ...string) error {
	return m.partialUpdate(ctx, userConsent, fields...)
}

func (m userConsentMock) Get(ctx context.Context, id int) (*model.UserConsent, error) {
	return m.get(ctx, id)
}

func (m userConsentMock) Insert(ctx context.Context, userConsent model.UserConsent) error {
	return nil
}

func (m userConsentMock) Delete(ctx context.Context, userConsent *model.UserConsent) error {
	return nil
}

func TestList(t *testing.T) {
	tests := map[string]struct {
		acc                  *ajwt.AccessClaims
		filters              model.UserConsentFilters
		userConsentList      func(context.Context, model.UserConsentFilters) ([]*model.UserConsent, error)
		expectedUserConsents []*model.UserConsent
		expectedErr          error
	}{
		"a call without jwt cannot list consents": {
			filters:     model.UserConsentFilters{UserID: "1"},
			expectedErr: merror.Forbidden().Describe("missing user scope or asking wrong user_id"),
		},
		"a call with JWT but without scope user cannot list consents": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: ""},
			filters:     model.UserConsentFilters{UserID: "1"},
			expectedErr: merror.Forbidden().Describe("missing user scope or asking wrong user_id"),
		},
		"a call with JWT and scope user cannot list another user's consents": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters:     model.UserConsentFilters{UserID: "2"},
			expectedErr: merror.Forbidden().Describe("missing user scope or asking wrong user_id"),
		},
		"a call with JWT and scope user can list user's consents": {
			acc:     &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters: model.UserConsentFilters{UserID: "1"},
			userConsentList: func(_ context.Context, filters model.UserConsentFilters) ([]*model.UserConsent, error) {
				return []*model.UserConsent{&model.UserConsent{ID: 1, UserID: "1", ApplicationPurposeID: 1},
					&model.UserConsent{ID: 2, UserID: "1", ApplicationPurposeID: 2}}, nil
			},
			expectedUserConsents: []*model.UserConsent{&model.UserConsent{ID: 1, UserID: "1", ApplicationPurposeID: 1},
				&model.UserConsent{ID: 2, UserID: "1", ApplicationPurposeID: 2}},
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := UserConsent{
				repo: &userConsentMock{
					list: test.userConsentList,
				},
			}

			// call function to test
			userConsents, err := service.List(ctx, test.filters)

			// check assertions
			assert.Equal(t, test.expectedUserConsents, userConsents)
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

func TestRevoke(t *testing.T) {
	tests := map[string]struct {
		acc             *ajwt.AccessClaims
		patchInfo       *patch.Info
		ucGet           func(context.Context, int) (*model.UserConsent, error)
		ucPartialUpdate func(_ context.Context, uc *model.UserConsent, fields ...string) error
		expectedErr     error
	}{
		"a call without jwt cannot revoke a consent": {
			patchInfo: &patch.Info{
				Input:  []string{"RevokedAt"},
				Output: []string{"revoked_at"},
				Model:  &model.UserConsent{ID: 23, RevokedAt: null.TimeFrom(time.Now())},
			},
			ucGet: func(_ context.Context, id int) (*model.UserConsent, error) {
				assert.Equalf(t, 23, id, "user consent get")
				return &model.UserConsent{UserID: "1"}, nil
			},
			expectedErr: merror.Forbidden().Describe("missing user scope or asking wrong user_id"),
		},
		"a call with JWT but without scope user cannot revoke a consent": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "application"},
			patchInfo: &patch.Info{
				Input:  []string{"RevokedAt"},
				Output: []string{"revoked_at"},
				Model:  &model.UserConsent{ID: 23, RevokedAt: null.TimeFrom(time.Now())},
			},
			ucGet: func(_ context.Context, id int) (*model.UserConsent, error) {
				assert.Equalf(t, 23, id, "user consent get")
				return &model.UserConsent{UserID: "1"}, nil
			},
			expectedErr: merror.Forbidden().Describe("missing user scope or asking wrong user_id"),
		},
		"a call with JWT but without scope user cannot revoke another user's consent": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			patchInfo: &patch.Info{
				Input:  []string{"RevokedAt"},
				Output: []string{"revoked_at"},
				Model:  &model.UserConsent{ID: 23, RevokedAt: null.TimeFrom(time.Now())},
			},
			ucGet: func(_ context.Context, id int) (*model.UserConsent, error) {
				assert.Equalf(t, 23, id, "user consent get")
				return &model.UserConsent{UserID: "2"}, nil
			},
			expectedErr: merror.Forbidden().Describe("missing user scope or asking wrong user_id"),
		},
		"a call with JWT and scope user can revoke its consent": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			patchInfo: &patch.Info{
				Input:  []string{"RevokedAt"},
				Output: []string{"revoked_at"},
				Model:  &model.UserConsent{ID: 23, RevokedAt: null.TimeFrom(time.Now())},
			},
			ucGet: func(_ context.Context, id int) (*model.UserConsent, error) {
				assert.Equalf(t, 23, id, "user consent get")
				return &model.UserConsent{UserID: "1"}, nil
			},
			ucPartialUpdate: func(_ context.Context, uc *model.UserConsent, fields ...string) error {
				assert.Equalf(t, 23, uc.ID, "revoke user consent")
				assert.Equalf(t, []string{"revoked_at"}, fields, "revoke user consent")
				return nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := UserConsent{
				repo: &userConsentMock{
					get:           test.ucGet,
					partialUpdate: test.ucPartialUpdate,
				},
			}

			// call function to test
			err := service.PartialUpdate(ctx, test.patchInfo)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
