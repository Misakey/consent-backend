package service

type ConsentManager struct {
	//
	// Repositories section
	accounts            userAccountRepo
	consents            ssoConsentRepo
	applicationPurposes applicationPurposeRepo
	userConsents        userConsentRepo
	ssoClients          ssoClientsRepo

	misadminID string

	// the URL user's agent should redirect to to consent new app accesses
	consentURL string
}

func NewConsentManager(
	accounts userAccountRepo, consents ssoConsentRepo, applicationPurposes applicationPurposeRepo, userConsents userConsentRepo, ssoClients ssoClientsRepo,
	consentURL string, misadminID string,
) *ConsentManager {
	return &ConsentManager{
		accounts:            accounts,
		consents:            consents,
		applicationPurposes: applicationPurposes,
		userConsents:        userConsents,
		consentURL:          consentURL,
		misadminID:          misadminID,
		ssoClients:          ssoClients,
	}
}
