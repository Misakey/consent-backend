package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/model"
)

type dataChannelMock struct {
	insert func(ctx context.Context, dataChannels []*model.DataChannel) error
	list   func(ctx context.Context, filters model.DataChannelFilters) ([]*model.DataChannel, error)
	get    func(ctx context.Context, id int) (*model.DataChannel, error)
}

// Insert
func (dm *dataChannelMock) Insert(ctx context.Context, dataChannels []*model.DataChannel) error {
	return dm.insert(ctx, dataChannels)
}

func (dm *dataChannelMock) List(ctx context.Context, filters model.DataChannelFilters) ([]*model.DataChannel, error) {
	return dm.list(ctx, filters)
}

func (dm *dataChannelMock) Get(ctx context.Context, id int) (*model.DataChannel, error) {
	return dm.get(ctx, id)
}

func TestCreateDataChannel(t *testing.T) {
	tests := map[string]struct {
		acc               *ajwt.AccessClaims
		dataChannels      []*model.DataChannel
		dataChannelInsert func(context.Context, []*model.DataChannel) error
		expectedErr       error
	}{
		"a call without jwt cannot create a data channel": {
			dataChannels: []*model.DataChannel{},
			expectedErr:  merror.Forbidden(),
		},
		"a call with JWT but without scope user cannot create a data channel": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: ""},
			dataChannels: []*model.DataChannel{
				&model.DataChannel{
					OwnerUserID: "2",
				},
			},
			expectedErr: merror.Forbidden().
				Detail("owner_user_id", merror.DVForbidden).
				Describef("cannot create data channel for 2"),
		},
		"a call with JWT and scope user cannot create a data channel if not the same user as the requester": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			dataChannels: []*model.DataChannel{
				&model.DataChannel{
					OwnerUserID: "2",
				},
			},
			expectedErr: merror.Forbidden().
				Detail("owner_user_id", merror.DVForbidden).
				Describef("cannot create data channel for 2"),
		},
		"a call with JWT and scope user can create a data channel if user is the same as the requester": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			dataChannels: []*model.DataChannel{
				&model.DataChannel{
					OwnerUserID: "1",
				},
			},
			dataChannelInsert: func(_ context.Context, dataChannels []*model.DataChannel) error {
				return nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := DataChannel{
				repo: &dataChannelMock{
					insert: test.dataChannelInsert,
				},
			}

			// call function to test
			err := service.Create(ctx, test.dataChannels)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

func TestListDataChannel(t *testing.T) {
	tests := map[string]struct {
		acc             *ajwt.AccessClaims
		filters         model.DataChannelFilters
		dataChannelList func(context.Context, model.DataChannelFilters) ([]*model.DataChannel, error)
		expectedErr     error
	}{
		"a call without jwt cannot list data channels": {
			filters:     model.DataChannelFilters{},
			expectedErr: merror.Forbidden(),
		},
		"a call with JWT but without scope user cannot list data channels": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: ""},
			filters: model.DataChannelFilters{
				OwnerUserID: "2",
			},
			expectedErr: merror.Forbidden().
				Detail("owner_user_id", merror.DVForbidden).
				Describef("cannot list data channels owner by 2"),
		},
		"a call with JWT and scope user cannot list data channels where the user not the owner of": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters: model.DataChannelFilters{
				OwnerUserID: "2",
			},
			expectedErr: merror.Forbidden().
				Detail("owner_user_id", merror.DVForbidden).
				Describef("cannot list data channels owner by 2"),
		},
		"a call with JWT and scope user can list data channels where the user is the owner of": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters: model.DataChannelFilters{
				OwnerUserID: "1",
			},
			dataChannelList: func(_ context.Context, filters model.DataChannelFilters) ([]*model.DataChannel, error) {
				return nil, nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := DataChannel{
				repo: &dataChannelMock{
					list: test.dataChannelList,
				},
			}

			// call function to test
			_, err := service.List(ctx, test.filters)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
