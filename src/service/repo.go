package service

import (
	"context"

	"gitlab.com/Misakey/consent-backend/src/model"
)

type purposeRepo interface {
	List(ctx context.Context) ([]*model.Purpose, error)
}

type applicationPurposeRepo interface {
	Insert(ctx context.Context, appPurpose *model.ApplicationPurpose) error
	Get(ctx context.Context, id int) (*model.ApplicationPurpose, error)
	List(ctx context.Context, filters model.ApplicationPurposeFilters) ([]*model.ApplicationPurpose, error)
	Delete(ctx context.Context, appPurpose *model.ApplicationPurpose) error
}

type ssoConsentRepo interface {
	GetInfo(ctx context.Context, challenge string) (*model.ConsentInfo, error)
	Accept(ctx context.Context, challenge string, acceptReq model.ConsentAcceptRequest) (model.ConsentAcceptInfo, error)
	Reject(ctx context.Context, challenge string, rejectReq model.RejectRequest) error
	List(ctx context.Context, id string) ([]model.ConsentInfoClient, error)
}

type userAccountRepo interface {
	Get(ctx context.Context, key string) (*model.UserAccount, error)
}

type applicationPurposeDatatypeRepo interface {
	Insert(ctx context.Context, appPurpose *model.ApplicationPurposeDatatype) error
	Get(ctx context.Context, id int) (*model.ApplicationPurposeDatatype, error)
	List(ctx context.Context, filters model.ApplicationPurposeDatatypeFilters) ([]*model.ApplicationPurposeDatatype, error)
	Delete(ctx context.Context, appPurposeDatatype *model.ApplicationPurposeDatatype) error
}

type ssoClientsRepo interface {
	Get(ctx context.Context, id string) (*model.SSOClient, error)
	Update(ctx context.Context, ssoClient *model.SSOClient) error
}

type userConsentRepo interface {
	Insert(ctx context.Context, userConsent model.UserConsent) error
	Get(ctx context.Context, id int) (*model.UserConsent, error)
	List(ctx context.Context, filters model.UserConsentFilters) ([]*model.UserConsent, error)
	PartialUpdate(ctx context.Context, userConsent *model.UserConsent, fields ...string) error
	Delete(ctx context.Context, userConsent *model.UserConsent) error
}

type dataChannelRepo interface {
	Insert(ctx context.Context, dataChannels []*model.DataChannel) error
	Get(ctx context.Context, id int) (*model.DataChannel, error)
	List(ctx context.Context, filters model.DataChannelFilters) ([]*model.DataChannel, error)
}

type cryptogramRepo interface {
	Insert(ctx context.Context, cryptogram *model.Cryptogram) error
	List(ctx context.Context, filters model.CryptogramlFilters) ([]*model.Cryptogram, error)
}

type applicationRepo interface {
	Get(ctx context.Context, id string) (*model.ApplicationInfo, error)
}
