package service

import (
	"context"
	"time"

	"github.com/volatiletech/null"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/consent-backend/src/model"
)

type UserConsent struct {
	repo userConsentRepo
}

func NewUserConsent(repo userConsentRepo) *UserConsent {
	return &UserConsent{
		repo: repo,
	}
}

// List user consents based on user ids and application ids
// Can only list their own consents
func (uc *UserConsent) List(ctx context.Context, filters model.UserConsentFilters) ([]*model.UserConsent, error) {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotUser(filters.UserID) {
		return nil, merror.Forbidden().Describe("missing user scope or asking wrong user_id")
	}
	return uc.repo.List(ctx, filters)
}

// PartialUpdate a user consent
// Can only update their own consent
func (uc *UserConsent) PartialUpdate(ctx context.Context, pi *patch.Info) error {
	patchedUserConsent := pi.Model.(*model.UserConsent)

	userConsent, err := uc.repo.Get(ctx, patchedUserConsent.ID)
	if err != nil {
		return err
	}

	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotUser(userConsent.UserID) {
		return merror.Forbidden().Describe("missing user scope or asking wrong user_id")
	}

	if userConsent.RevokedAt.Valid {
		return merror.Conflict().Describe("user consent already revoked").Detail("revoked_at", merror.DVConflict)
	}

	patchedUserConsent.RevokedAt = null.TimeFrom(time.Now())

	// fields we want to patch
	pi.Whitelist([]string{"RevokedAt"})
	return uc.repo.PartialUpdate(ctx, patchedUserConsent, pi.Output...)
}
