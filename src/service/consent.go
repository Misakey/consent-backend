package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/model"
)

type Consent struct {
	ssoConsentRepo  ssoConsentRepo
	userConsentRepo userConsentRepo
}

func NewConsent(ssoConsentRepo ssoConsentRepo, userConsentRepo userConsentRepo) *Consent {
	return &Consent{
		ssoConsentRepo:  ssoConsentRepo,
		userConsentRepo: userConsentRepo,
	}
}

// Delete user consents based on user ids
// Can only delete his own consents
func (c *Consent) Delete(ctx context.Context, userID string) error {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotUser(userID) {
		return merror.Forbidden().Describe("missing user scope or wrong user_id")
	}

	userConsents, err := c.userConsentRepo.List(ctx, model.UserConsentFilters{UserID: userID})
	if err != nil {
		return err
	}

	for _, userConsent := range userConsents {
		err := c.userConsentRepo.Delete(ctx, userConsent)
		if err != nil {
			return err
		}
	}

	return err
}
