package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/model"
)

// ApplicationPurpose service layer
type ApplicationPurpose struct {
	repo applicationPurposeRepo
}

// ApplicationPurpose constructor
func NewApplicationPurpose(repo applicationPurposeRepo) *ApplicationPurpose {
	return &ApplicationPurpose{
		repo: repo,
	}
}

// Create an application-purpose
// User has to have a `user` scope and admin role on the application
func (ap *ApplicationPurpose) Create(ctx context.Context, appPurpose *model.ApplicationPurpose) error {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotAdminOn(appPurpose.ApplicationID) {
		return merror.Forbidden().Describe("missing user scope or not admin")
	}
	return ap.repo.Insert(ctx, appPurpose)
}

// List application-purposes based on application-purpose ids, application ids or purpose ids
// No access check required (public route)
func (ap *ApplicationPurpose) List(ctx context.Context, filters model.ApplicationPurposeFilters) ([]*model.ApplicationPurpose, error) {
	return ap.repo.List(ctx, filters)
}

// Delete an application-purpose
// User has to have a `user` scope and admin role on the application
func (ap *ApplicationPurpose) Delete(ctx context.Context, id int) error {
	appPurpose, err := ap.repo.Get(ctx, id)
	if err != nil {
		return err
	}

	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotAdminOn(appPurpose.ApplicationID) {
		return merror.Forbidden().Describe("missing user scope or not admin")
	}

	return ap.repo.Delete(ctx, appPurpose)
}
