package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/consent-backend/src/model"
)

func TestComputeFinalScopes(t *testing.T) {
	tests := map[string]struct {
		consentedAppPurposes []*model.ApplicationPurpose
		initialScopes        []string
		expectedScopes       []string
	}{
		"empty consented app purposes should remove all purposes scopes": {
			consentedAppPurposes: []*model.ApplicationPurpose{},
			initialScopes:        []string{"openid", "user", "pur.data_science"},
			expectedScopes:       []string{"openid", "user"},
		},
		"partial consented app purposes should remove not consented purposes scopes": {
			consentedAppPurposes: []*model.ApplicationPurpose{
				&model.ApplicationPurpose{PurposeLabel: "minimum_required"},
			},
			initialScopes:  []string{"openid", "user", "pur.minimum_required", "pur.data_science"},
			expectedScopes: []string{"openid", "user", "pur.minimum_required"},
		},
	}

	for description, test := range tests {
		t.Run(description, func(t *testing.T) {
			// init service with mocked sub-layers
			service := ConsentManager{}

			// call function to test
			result := service.computeFinalScopes(test.consentedAppPurposes, test.initialScopes)

			// check assertions
			assert.Equal(t, test.expectedScopes, result)
		})

	}
}
