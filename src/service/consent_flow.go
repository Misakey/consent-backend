package service

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"strings"

	"github.com/volatiletech/null"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/auth"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

// InitUserConsents initiates a consent flow between the app and the user
func (am *ConsentManager) InitUserConsents(ctx context.Context, challenge string) string {
	if len(challenge) == 0 {
		return auth.BuildRedirectErr(merror.BadRequestCode, "challenge cannot be empty", am.consentURL)
	}

	// retrieve info from hydra about the current consent
	c, err := am.consents.GetInfo(ctx, challenge)
	if err != nil {
		return auth.BuildRedirectErr(merror.InvalidFlowCode, err.Error(), am.consentURL)
	}

	// NOTE:  we ignore c.Skip of hydra since consent are handled on our side

	// get user info to use it further
	user, err := am.accounts.Get(ctx, c.Subject)
	if err != nil {
		return am.rejectConsent(ctx, challenge, err.Error())
	}

	// check scopes + retrieve requested application purposes ids
	appPurposeIDs, err := am.checkAuthZScopes(ctx, c.Info.ClientID, c.RequestedScope)
	if err != nil {
		return am.rejectConsent(ctx, challenge, err.Error())
	}

	var consents []*model.UserConsent
	if len(appPurposeIDs) > 0 {
		// check if user already consented to all requested application purposes
		filters := model.UserConsentFilters{
			UserID:                   c.Subject,
			ApplicationPurposeIDs:    appPurposeIDs,
			Revoked:                  false,
			UniqueApplicationPurpose: null.BoolFrom(true),
		}
		consents, err = am.userConsents.List(ctx, filters)
		if err != nil {
			return am.rejectConsent(ctx, challenge, err.Error())
		}
	}

	// remove already consented application purposes
	for _, consent := range consents {
		appPurposeIDs = slices.RemoveIntFromArray(appPurposeIDs, consent.ApplicationPurposeID)
	}

	// if no consent is required: we accept directly the consent
	if len(appPurposeIDs) == 0 {
		return am.acceptConsent(ctx, challenge, c.RequestedScope)
	}

	// otherwise - built consent url and return it
	finalURL, err := url.ParseRequestURI(am.consentURL)
	if err != nil {
		return am.rejectConsent(ctx, challenge, err.Error())
	}

	// redirect url building to consent page:
	// add requested application purposes ids
	params := url.Values{}
	if len(appPurposeIDs) > 0 {
		params.Add("application_purpose_ids", slices.JoinInt(appPurposeIDs, ","))
	}
	// add consent_challenge
	params.Set("consent_challenge", challenge)
	// add sso client info
	params.Set("client_name", c.Info.AppName)
	params.Set("client_id", c.Info.ClientID)
	params.Set("logo_uri", c.Info.LogoURI)
	// add user info
	params.Set("email", user.Email)
	params.Set("display_name", user.DisplayName)
	params.Set("avatar_uri", user.AvatarURI.String)

	finalURL.RawQuery = params.Encode() // escape query parameters
	return finalURL.String()
}

// SetUserConsents : final step of the consent flow - user accepts consent and we notify hydra about it then return a redirect url
func (am *ConsentManager) SetUserConsents(ctx context.Context, consents model.UserDataConsents) (*model.ConsentAcceptInfo, error) {
	// 1. retrieve information about current consent
	// retrieve info from hydra
	c, err := am.consents.GetInfo(ctx, consents.Challenge)
	if err != nil {
		return nil, merror.BadRequest().From(merror.OriBody).
			Describe(err.Error()).Detail("consent_challenge", merror.DVInvalid)
	}

	// get user info
	user, err := am.accounts.Get(ctx, c.Subject)
	if err != nil {
		_ = am.rejectConsent(ctx, consents.Challenge, err.Error())
		return nil, merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	// 2. if new consents are here, we verify and create them
	if len(consents.ApplicationPurposeIDs) != 0 {
		if err := am.createUserConsents(ctx, consents, c.RequestedScope, user.ID, c.Info.ClientID); err != nil {
			_ = am.rejectConsent(ctx, consents.Challenge, err.Error())
			return nil, err
		}
	}

	// 3. accept all requested scopes to hydra that have been consented previously or during this consent request
	// first retrieve all user consents for the given application (it retrieve the ones we just have created)
	userConsents, err := am.userConsents.List(
		ctx,
		model.UserConsentFilters{
			UserID:                   user.ID,
			ApplicationIDs:           []string{c.Info.ClientID},
			Revoked:                  false,
			UniqueApplicationPurpose: null.BoolFrom(true),
		},
	)
	if err != nil {
		_ = am.rejectConsent(ctx, consents.Challenge, err.Error())
		return nil, err
	}

	// build the list of application purpose ids
	var consentedAppPurposeIDs []int
	for _, userConsent := range userConsents {
		consentedAppPurposeIDs = append(consentedAppPurposeIDs, userConsent.ApplicationPurposeID)
	}

	// retrieve application purposes
	var consentedAppPurposes []*model.ApplicationPurpose
	if len(consentedAppPurposeIDs) != 0 {
		// get final list of consented application purposes considering all existing user consents
		consentedAppPurposes, err = am.applicationPurposes.List(
			ctx,
			model.ApplicationPurposeFilters{IDs: consentedAppPurposeIDs},
		)
		if err != nil {
			_ = am.rejectConsent(ctx, consents.Challenge, err.Error())
			return nil, merror.BadRequest().From(merror.OriBody).Describe(err.Error())
		}
	}

	// get final list of scopes
	finalScopes := am.computeFinalScopes(consentedAppPurposes, c.RequestedScope)

	// send accept consent request on hydra
	url := am.acceptConsent(ctx, consents.Challenge, finalScopes)
	return &model.ConsentAcceptInfo{RedirectTo: url}, nil
}

// createUserConsents verifying first all application purposes
func (am *ConsentManager) createUserConsents(
	ctx context.Context,
	consents model.UserDataConsents,
	initialScopes []string,
	userID string,
	appID string,
) error {
	// list application purposes to check they do all exist for given appID
	filters := model.ApplicationPurposeFilters{
		IDs:    consents.ApplicationPurposeIDs,
		AppIDs: []string{appID},
	}
	appPurposes, err := am.applicationPurposes.List(ctx, filters)
	if err != nil {
		_ = am.rejectConsent(ctx, consents.Challenge, err.Error())
		return merror.BadRequest().From(merror.OriBody).Describe((err.Error()))
	}
	// if we retrieve less application purposes than the one consented, something is wrong with the request
	if len(appPurposes) != len(consents.ApplicationPurposeIDs) {
		err := errors.New("at least one requested application purpose has been not found")
		_ = am.rejectConsent(ctx, consents.Challenge, err.Error())
		return merror.Forbidden().From(merror.OriBody).Describe((err.Error()))
	}

	// verify all application purposes are part of initial request scopes
	for _, appPurpose := range appPurposes {
		purScope := ajwt.BuildPurposeScope(appPurpose.PurposeLabel)
		if !slices.ContainsString(initialScopes, purScope) {
			err := fmt.Errorf("%s is not part of initial requested scopes", purScope)
			_ = am.rejectConsent(ctx, consents.Challenge, err.Error())
			return merror.Forbidden().
				From(merror.OriBody).
				Describe(err.Error()).
				Detail("application_purpose_ids", merror.DVInvalid)
		}
	}

	// create user consents
	for _, appPurposeID := range consents.ApplicationPurposeIDs {
		userConsent := model.UserConsent{
			UserID:               userID,
			ApplicationPurposeID: appPurposeID,
		}
		if err := am.userConsents.Insert(ctx, userConsent); err != nil {
			return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
		}
	}
	return nil
}

// computeFinalScopes by excluding purpose scope missing from consentedAppPurposes list
func (am *ConsentManager) computeFinalScopes(
	consentedAppPurposes []*model.ApplicationPurpose,
	initialScopes []string,
) []string {
	// translate consented app purposes into a slice of purposes scopes corresponding to it
	consentedPurScopes := make([]string, len(consentedAppPurposes))
	for i, appPurpose := range consentedAppPurposes {
		consentedPurScopes[i] = ajwt.BuildPurposeScope(appPurpose.PurposeLabel)
	}

	// remove unconsented purpose scopes from initially requested scopes
	for _, scope := range initialScopes {
		// ensure we remove only purpose scopes using IsPurposeScope (we want to keep openid/user scope for instance)
		if ajwt.IsPurposeScope(scope) && !slices.ContainsString(consentedPurScopes, scope) {
			initialScopes = slices.RemoveStringFromArray(initialScopes, scope)
		}
	}
	return initialScopes
}

// checkAuthZScopes controls all scopes related to authorization:
// 1. check each application purposes does exist for current client id
// the function returns the application purposes ids that user has the choice to consent.
func (am *ConsentManager) checkAuthZScopes(
	ctx context.Context,
	clientID string, scopes []string,
) ([]int, error) {
	// 1. check each application purpose label existing for given client ID (appID)
	appPurposeLabels, err := am.buildApplicationPurposeLabels(scopes)
	if err != nil {
		return nil, err
	}
	// if no application purposes has been asked, we directly return an empty array
	if len(appPurposeLabels) == 0 {
		return nil, nil
	}
	appPurposes, err := am.applicationPurposes.List(ctx, model.ApplicationPurposeFilters{
		PurposeLabels: appPurposeLabels,
		AppIDs:        []string{clientID},
	})
	if err != nil {
		return nil, err
	}

	// if length of retrieve application purposes has not same size of requested application purpose labels, there is an issue
	if len(appPurposes) != len(appPurposeLabels) {
		return nil, errors.New("at least one requested application purpose has been not found")
	}

	// create a list of application_purpose ids from list of retrieved application purpose
	var appPurposeIDs []int
	for _, appPurpose := range appPurposes {
		appPurposeIDs = append(appPurposeIDs, appPurpose.ID)
	}
	return appPurposeIDs, nil
}

// buildApplicationPurposeLabels removing duplicates
func (am *ConsentManager) buildApplicationPurposeLabels(scopes []string) ([]string, error) {
	// list of application purposes labels built on the fly checking scopes
	// used to check in one request existence of application purpose labels in storage for given clientID (appID)
	applicationPurposeLabels := []string{}
	// map used to ensure uniqueness of application purposes labels in our slice
	uniqueApplicationPurposeLabels := make(map[string]struct{})

	// check scopes
	for _, sco := range scopes {
		if !ajwt.IsAllowedScope(sco) {
			return nil, merror.Forbidden().Describef("unknown scope %s", sco).Detail("scope", merror.DVForbidden)
		}
		if ajwt.IsPurposeScope(sco) {
			applicationPurposeLabel := ajwt.GetPurpose(sco)
			_, ok := uniqueApplicationPurposeLabels[applicationPurposeLabel]
			if ok {
				continue // do not append if existing
			}
			applicationPurposeLabels = append(applicationPurposeLabels, applicationPurposeLabel)
			uniqueApplicationPurposeLabels[applicationPurposeLabel] = struct{}{}
		}
	}
	return applicationPurposeLabels, nil
}

func (am *ConsentManager) acceptConsent(
	ctx context.Context,
	challenge string,
	scopes []string) string {
	// add scopes in ID Token
	session := model.Session{}
	session.IDToken.Scope = strings.Join(scopes, " ")

	// build consent accept request
	accReq := model.ConsentAcceptRequest{
		GrantScope:  scopes,
		Remember:    true,
		RememberFor: 0,
		Session:     session,
	}
	accInfo, err := am.consents.Accept(ctx, challenge, accReq)
	if err != nil {
		return auth.BuildRedirectErr(merror.InvalidFlowCode, err.Error(), am.consentURL)
	}
	return accInfo.RedirectTo
}

func (am *ConsentManager) rejectConsent(ctx context.Context, challenge string, reason string) string {
	req := model.RejectRequest{ErrorDescription: reason}
	if err := am.consents.Reject(ctx, challenge, req); err != nil {
		return auth.BuildRedirectErr(merror.InvalidFlowCode, err.Error(), am.consentURL)
	}
	return auth.BuildRedirectErr(merror.InvalidFlowCode, reason, am.consentURL)
}
