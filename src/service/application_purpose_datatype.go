package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/model"
)

// ApplicationPurposeDatatype service layer
type ApplicationPurposeDatatype struct {
	repo           applicationPurposeDatatypeRepo
	appPurposeRepo applicationPurposeRepo
}

// ApplicationPurposeDatatype constructor
func NewApplicationPurposeDatatype(repo applicationPurposeDatatypeRepo, appPurposeRepo applicationPurposeRepo) *ApplicationPurposeDatatype {
	return &ApplicationPurposeDatatype{
		repo:           repo,
		appPurposeRepo: appPurposeRepo,
	}
}

// Create an application-purpose-datatype
// User has to have a `user` scope and admin role on the application
func (ap *ApplicationPurposeDatatype) Create(ctx context.Context, appPurposeDatatype *model.ApplicationPurposeDatatype) error {
	// we get application purpose to check if user is admin of the application
	appPurpose, err := ap.appPurposeRepo.Get(ctx, appPurposeDatatype.ApplicationPurposeID)
	if err != nil {
		return err
	}

	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotAdminOn(appPurpose.ApplicationID) {
		return merror.Forbidden().Describe("missing user scope or not admin")
	}

	return ap.repo.Insert(ctx, appPurposeDatatype)
}

// List application-purposes-datatype based on application-purpose-datatype ids
// No access check required (public route)
func (ap *ApplicationPurposeDatatype) List(ctx context.Context, filters model.ApplicationPurposeDatatypeFilters) ([]*model.ApplicationPurposeDatatype, error) {
	return ap.repo.List(ctx, filters)
}

// Delete an application-purpose-datatype
// User has to have a `user` scope and admin role on the application
func (ap *ApplicationPurposeDatatype) Delete(ctx context.Context, id int) error {
	appPurposeDatatype, err := ap.repo.Get(ctx, id)
	if err != nil {
		return err
	}

	// we get application purpose to check if user is admin of the application
	appPurpose, err := ap.appPurposeRepo.Get(ctx, appPurposeDatatype.ApplicationPurposeID)
	if err != nil {
		return err
	}

	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotAdminOn(appPurpose.ApplicationID) {
		return merror.Forbidden().Describe("missing user scope or not admin")
	}

	return ap.repo.Delete(ctx, appPurposeDatatype)

}
