package service

import (
	"context"
	"testing"

	"gitlab.com/Misakey/consent-backend/src/model"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

type applicationInfoMock struct {
	get func(ctx context.Context, id string) (*model.ApplicationInfo, error)
}

func (am *applicationInfoMock) Get(ctx context.Context, id string) (*model.ApplicationInfo, error) {
	return am.get(ctx, id)
}

type cryptogramtMock struct {
	insert func(ctx context.Context, cryptogram *model.Cryptogram) error
	list   func(ctx context.Context, filters model.CryptogramlFilters) ([]*model.Cryptogram, error)
}

// Insert
func (cm *cryptogramtMock) Insert(ctx context.Context, cryptogram *model.Cryptogram) error {
	return cm.insert(ctx, cryptogram)
}

func (cm *cryptogramtMock) List(ctx context.Context, filters model.CryptogramlFilters) ([]*model.Cryptogram, error) {
	return cm.list(ctx, filters)
}

func TestCreateCryptogram(t *testing.T) {
	tests := map[string]struct {
		acc                *ajwt.AccessClaims
		cryptogram         *model.Cryptogram
		cryptogramInsert   func(context.Context, *model.Cryptogram) error
		dataChannelGet     func(context.Context, int) (*model.DataChannel, error)
		applicationInfoGet func(context.Context, string) (*model.ApplicationInfo, error)
		expectedCryptogram *model.Cryptogram
		expectedErr        error
	}{
		"a call without jwt cannot create a cryptogram": {
			cryptogram: &model.Cryptogram{DataChannelID: 1},
			dataChannelGet: func(_ context.Context, id int) (*model.DataChannel, error) {
				return &model.DataChannel{
					ID:          1,
					OwnerUserID: "2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with JWT but without scope user cannot create a cryptogram": {
			acc:        &ajwt.AccessClaims{Subject: "1", Scope: ""},
			cryptogram: &model.Cryptogram{DataChannelID: 1},
			dataChannelGet: func(_ context.Context, id int) (*model.DataChannel, error) {
				return &model.DataChannel{
					ID:          1,
					OwnerUserID: "2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with JWT and scope user cannot create a cryptogram where the user is not the owner of the datachannel": {
			acc:        &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			cryptogram: &model.Cryptogram{ProducerApplicationID: "uuid_1", DataChannelID: 1},
			dataChannelGet: func(_ context.Context, id int) (*model.DataChannel, error) {
				return &model.DataChannel{
					ID:          1,
					OwnerUserID: "2",
				}, nil
			},
			applicationInfoGet: func(_ context.Context, id string) (*model.ApplicationInfo, error) {
				assert.Equal(t, id, "uuid_1")
				return &model.ApplicationInfo{ID: "uuid_1"}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with JWT and scope user cannot create a cryptogram where the requested producer application does not exit": {
			acc:        &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			cryptogram: &model.Cryptogram{ProducerApplicationID: "uuid_2", DataChannelID: 1},
			dataChannelGet: func(_ context.Context, id int) (*model.DataChannel, error) {
				return &model.DataChannel{
					ID:          1,
					OwnerUserID: "1",
				}, nil
			},
			applicationInfoGet: func(_ context.Context, id string) (*model.ApplicationInfo, error) {
				assert.Equal(t, id, "uuid_2")
				return nil, merror.NotFound().
					Detail("producer_application_id", merror.DVNotFound).
					Describef("application uuid_2 does not exist")
			},
			expectedErr: merror.NotFound().
				Detail("producer_application_id", merror.DVNotFound).
				Describef("application uuid_2 does not exist"),
		},
		"a call with JWT and scope user can create a cryptogram where the user is the owner of the datachannel": {
			acc:        &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			cryptogram: &model.Cryptogram{ProducerApplicationID: "uuid_1", DataChannelID: 1},
			dataChannelGet: func(_ context.Context, id int) (*model.DataChannel, error) {
				return &model.DataChannel{
					ID:          1,
					OwnerUserID: "1",
				}, nil
			},
			applicationInfoGet: func(_ context.Context, id string) (*model.ApplicationInfo, error) {
				assert.Equal(t, id, "uuid_1")
				return &model.ApplicationInfo{ID: "uuid_1"}, nil
			},
			cryptogramInsert: func(_ context.Context, cryptogram *model.Cryptogram) error {
				assert.Equal(t, cryptogram.DataChannelID, 1)
				assert.Equal(t, cryptogram.ProducerApplicationID, "uuid_1")
				return nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := Cryptogram{
				repo: &cryptogramtMock{
					insert: test.cryptogramInsert,
				},
				dataChannelRepo: &dataChannelMock{
					get: test.dataChannelGet,
				},
				appRepo: &applicationInfoMock{
					get: test.applicationInfoGet,
				},
			}

			// call function to test
			err := service.Create(ctx, test.cryptogram)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

func TestListCryptogram(t *testing.T) {
	tests := map[string]struct {
		acc            *ajwt.AccessClaims
		filters        model.CryptogramlFilters
		cryptogramList func(context.Context, model.CryptogramlFilters) ([]*model.Cryptogram, error)
		expectedErr    error
	}{
		"a call without jwt cannot list cryptograms": {
			filters:     model.CryptogramlFilters{},
			expectedErr: merror.Forbidden(),
		},
		"a call with JWT but without scope user cannot list cryptograms": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: ""},
			filters: model.CryptogramlFilters{
				OwnerUserID: "2",
			},
			expectedErr: merror.Forbidden().
				Describef("cannot access 2 cryptograms").
				Detail("owner_user_id", merror.DVForbidden),
		},
		"a call with JWT and scope user cannot list cryptograms where the user is not the owner of the datachannel": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters: model.CryptogramlFilters{
				OwnerUserID: "2",
			},
			expectedErr: merror.Forbidden().
				Describef("cannot access 2 cryptograms").
				Detail("owner_user_id", merror.DVForbidden),
		},
		"a call with JWT and scope user can list cryptograms where the user is the owner of the datachannel": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters: model.CryptogramlFilters{
				OwnerUserID: "1",
			},
			cryptogramList: func(_ context.Context, filters model.CryptogramlFilters) ([]*model.Cryptogram, error) {
				return nil, nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := Cryptogram{
				repo: &cryptogramtMock{
					list: test.cryptogramList,
				},
			}

			// call function to test
			_, err := service.List(ctx, test.filters)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
