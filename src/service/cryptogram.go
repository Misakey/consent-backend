package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/model"
)

// Cryptogram service layer
type Cryptogram struct {
	repo            cryptogramRepo
	appRepo         applicationRepo
	dataChannelRepo dataChannelRepo
}

// Cryptogram constructor
func NewCryptogram(repo cryptogramRepo, appRepo applicationRepo, dataChannelRepo dataChannelRepo) *Cryptogram {
	return &Cryptogram{
		repo:            repo,
		appRepo:         appRepo,
		dataChannelRepo: dataChannelRepo,
	}
}

// Create a cryptogram
// User must have a `user` scope
func (c *Cryptogram) Create(ctx context.Context, cryptogram *model.Cryptogram) error {
	dataChannel, err := c.dataChannelRepo.Get(ctx, cryptogram.DataChannelID)
	if err != nil {
		if merror.HasCode(err, merror.NotFoundCode) {
			// TODO: create a wipe detail method in msk
			return merror.NotFound().
				Detail("data_channel_id", merror.DVNotFound).
				Describef("data channel %d does not exist", cryptogram.DataChannelID)
		}
		return err
	}
	// TODO: update access right when applications will be authorize to
	// create cryptograms (post google-takeout)
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotUser(dataChannel.OwnerUserID) {
		return merror.Forbidden()
	}

	// verify if application_id exists
	_, err = c.appRepo.Get(ctx, cryptogram.ProducerApplicationID)
	if err != nil {
		if merror.HasCode(err, merror.NotFoundCode) {
			return merror.NotFound().
				Detail("producer_application_id", merror.DVNotFound).
				Describef("application %s does not exist", cryptogram.ProducerApplicationID)
		}
		return err
	}

	return c.repo.Insert(ctx, cryptogram)
}

func (c *Cryptogram) List(ctx context.Context, filters model.CryptogramlFilters) ([]*model.Cryptogram, error) {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return nil, merror.Forbidden()
	}
	if acc.IsNotUser(filters.OwnerUserID) {
		return nil, merror.Forbidden().
			Describef("cannot access %s cryptograms", filters.OwnerUserID).
			Detail("owner_user_id", merror.DVForbidden)
	}

	return c.repo.List(ctx, filters)

}
