package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190807101651, Down20190807101651)
}

func Up20190807101651(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE application_purpose(
        id SERIAL PRIMARY KEY,
        purpose_id INT NOT NULL REFERENCES purpose ON DELETE CASCADE,
        application_id UUID NOT NULL,
        UNIQUE (purpose_id, application_id)
   );`)
	return err
}

func Down20190807101651(tx *sql.Tx) error {
	_, err := tx.Exec(`
		DROP TABLE application_purpose;
   `)
	return err
}
