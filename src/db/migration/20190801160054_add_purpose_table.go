package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190801160054, Down20190801160054)
}

func Up20190801160054(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE purpose(
		id SERIAL PRIMARY KEY,
    label VARCHAR(255) NOT NULL
   );`)
	if err != nil {
		return fmt.Errorf("purpose table: %v", err)
	}

	// Insert default purposes.
	_, err = tx.Exec(`
		INSERT INTO purpose (id, label)
		VALUES
       (1, 'minimum_required'),
       (2, 'service_personalization'),
       (3, 'security_improvement'),
       (4, 'user_profiling'),
       (5, 'marketing_communication'),
       (6, 'provision_to_partners'),
       (7, 'data_science');
   `)

	return err
}

func Down20190801160054(tx *sql.Tx) error {
	_, err := tx.Exec(`
		DROP TABLE purpose;
   `)
	return err
}
