package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(upAddCryptogramTable, downAddCryptogramTable)
}

func upAddCryptogramTable(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TYPE CRYPTOGRAM_DATA_SOURCE_ENUM AS ENUM(
		'takeout_google_pipeline_v_1'
	);`)
	if err != nil {
		return fmt.Errorf("creating cryptogram data source enum: %v", err)
	}

	_, err = tx.Exec(`CREATE TABLE cryptogram(
	    id SERIAL PRIMARY KEY,
	    data_channel_id INTEGER NOT NULL REFERENCES data_channel ON DELETE CASCADE,
	    producer_application_id UUID NOT NULL,
	    data_source CRYPTOGRAM_DATA_SOURCE_ENUM NOT NULL,
	    content VARCHAR(1023) NOT NULL, 
	    data_timestamp timestamptz NOT NULL
	);`)
	if err != nil {
		return fmt.Errorf("creating cryptogram table: %v", err)
	}

	return err
}

func downAddCryptogramTable(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE cryptogram;`)
	if err != nil {
		return fmt.Errorf("dropping cryptogram table: %v", err)
	}

	_, err = tx.Exec(`DROP TYPE CRYPTOGRAM_DATA_SOURCE_ENUM;`)
	if err != nil {
		return fmt.Errorf("dropping cryptogram data source enum: %v", err)
	}

	return err
}
