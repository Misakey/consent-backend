package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190808102509, Down20190808102509)
}

func Up20190808102509(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE application_purpose_datatype(
        id SERIAL PRIMARY KEY,
        datatype_id INT NOT NULL REFERENCES datatype ON DELETE CASCADE,
        consent_time_value INT NOT NULL,
        application_purpose_id INT NOT NULL REFERENCES application_purpose ON DELETE CASCADE,
        UNIQUE (datatype_id, application_purpose_id)
   );`)
	return err

}

func Down20190808102509(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE application_purpose_datatype`)
	return err
}
