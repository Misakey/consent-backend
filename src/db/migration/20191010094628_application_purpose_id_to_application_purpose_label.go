package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20191010094628, Down20191010094628)
}

func Up20191010094628(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TYPE PURPOSE_LABEL_ENUM AS ENUM(
		'minimum_required',
		'service_personalization',
		'security_improvement',
		'user_profiling',
		'marketing_communication',
		'provision_to_partners',
		'data_science'
	);`)
	if err != nil {
		return fmt.Errorf("creating purpose label enum: %v", err)
	}

	// Replace existing value by PURPOSE_LABEL_ENUM
	_, err = tx.Exec(`ALTER TABLE purpose
		ADD label_tmp PURPOSE_LABEL_ENUM
	;`)
	if err != nil {
		return fmt.Errorf("adding column label_tmp in table purpose: %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'minimum_required'
		WHERE label = 'minimum_required'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'minimum_required': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'service_personalization'
		WHERE label = 'service_personalization'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'service_personalization': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'security_improvement'
		WHERE label = 'security_improvement'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'security_improvement': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'user_profiling'
		WHERE label = 'user_profiling'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'user_profiling': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'marketing_communication'
		WHERE label = 'marketing_communication'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'marketing_communication': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'provision_to_partners'
		WHERE label = 'provision_to_partners'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'provision_to_partners': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'data_science'
		WHERE label = 'data_science'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'data_science': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE purpose
		ALTER COLUMN label_tmp SET NOT NULL
	;`)
	if err != nil {
		return fmt.Errorf("adding 'not null' constraint on purpose label: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE purpose
		DROP label
	;`)
	if err != nil {
		return fmt.Errorf("dropping 'label' column: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE purpose
		RENAME label_tmp TO label
	;`)
	if err != nil {
		return fmt.Errorf("renaming column to 'label': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE purpose
		ADD CONSTRAINT purpose_label_key UNIQUE (label)
	;`)
	if err != nil {
		return fmt.Errorf("adding 'unique' constraint on purpose label: %v", err)
	}

	// Replace application_purpose purpose_id by PURPOSE_LABEL_ENUM
	_, err = tx.Exec(`ALTER TABLE application_purpose
		ADD purpose_label PURPOSE_LABEL_ENUM
	;`)
	if err != nil {
		return fmt.Errorf("adding column purpose_label in table application_purpose: %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_label = 'minimum_required'
		WHERE purpose_id = 1
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'minimum_required': %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_label = 'service_personalization'
		WHERE purpose_id = 2
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'service_personalization': %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_label = 'security_improvement'
		WHERE purpose_id = 3
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'security_improvement': %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_label = 'user_profiling'
		WHERE purpose_id = 4
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'user_profiling': %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_label = 'marketing_communication'
		WHERE purpose_id = 5
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'marketing_communication': %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_label = 'provision_to_partners'
		WHERE purpose_id = 6
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'provision_to_partners': %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_label = 'data_science'
		WHERE purpose_id = 7
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'data_science': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE application_purpose
		ALTER COLUMN purpose_label SET NOT NULL
	;`)
	if err != nil {
		return fmt.Errorf("adding 'not null' constraint on purpose label: %v", err)
	}

	// Drop contraint hold between purpose_id & application_id
	_, err = tx.Exec(`ALTER TABLE application_purpose
		DROP CONSTRAINT application_purpose_purpose_id_application_id_key;`)
	if err != nil {
		return fmt.Errorf("fail to drop constraint application_purpose_purpose_id_application_id_key': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE application_purpose
		DROP purpose_id
	;`)
	if err != nil {
		return fmt.Errorf("dropping 'purpose_id' column: %v", err)
	}

	// Add contraint between purpose_label & application_id
	_, err = tx.Exec(`ALTER TABLE application_purpose
		ADD CONSTRAINT application_purpose_purpose_label_application_id_key UNIQUE (purpose_label,application_id)
	;`)
	if err != nil {
		return fmt.Errorf("fail to drop constraint application_purpose_purpose_label_application_id_key': %v", err)
	}

	// Add foreign key between purpose_label & purpose table
	_, err = tx.Exec(`ALTER TABLE application_purpose
		ADD CONSTRAINT application_purpose_purpose_label_key FOREIGN KEY (purpose_label) REFERENCES purpose (label) MATCH FULL
	;`)
	if err != nil {
		return fmt.Errorf("add constraint foreign key application_purpose_purpose_id_key': %v", err)
	}

	return nil

}

func Down20191010094628(tx *sql.Tx) error {
	// Replace existing value by label varchar
	_, err := tx.Exec(`ALTER TABLE purpose
		ADD label_tmp VARCHAR(255) UNIQUE
	;`)
	if err != nil {
		return fmt.Errorf("adding column label_tmp in table purpose: %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'minimum_required'
		WHERE label = 'minimum_required'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'minimum_required': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'service_personalization'
		WHERE label = 'service_personalization'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'service_personalization': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'security_improvement'
		WHERE label = 'security_improvement'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'security_improvement': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'user_profiling'
		WHERE label = 'user_profiling'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'user_profiling': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'marketing_communication'
		WHERE label = 'marketing_communication'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'marketing_communication': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'provision_to_partners'
		WHERE label = 'provision_to_partners'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'provision_to_partners': %v", err)
	}

	_, err = tx.Exec(`UPDATE purpose
		SET label_tmp = 'data_science'
		WHERE label = 'data_science'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'data_science': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE purpose
		ALTER COLUMN label_tmp SET NOT NULL
	;`)
	if err != nil {
		return fmt.Errorf("adding 'not null' constraint on purpose label: %v", err)
	}

	// Drop reference of purpose_label on application_purpose
	_, err = tx.Exec(`ALTER TABLE application_purpose
		DROP CONSTRAINT application_purpose_purpose_label_key;`)
	if err != nil {
		return fmt.Errorf("fail to drop constraint application_purpose_purpose_label_key': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE purpose
		DROP label
	;`)
	if err != nil {
		return fmt.Errorf("dropping 'label' column: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE purpose
		RENAME label_tmp TO label
	;`)
	if err != nil {
		return fmt.Errorf("renaming column to 'label': %v", err)
	}

	// Replace application_purpose purpose_label by purpose_id
	_, err = tx.Exec(`ALTER TABLE application_purpose
		ADD purpose_id INT
	;`)
	if err != nil {
		return fmt.Errorf("adding column prupose_id in table application_purpose: %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_id = 1
		WHERE purpose_label = 'minimum_required'
	;`)
	if err != nil {
		return fmt.Errorf("setting purpose_id: %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_id = 2
		WHERE purpose_label = 'service_personalization'
	;`)
	if err != nil {
		return fmt.Errorf("setting purpose_id: %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_id = 3
		WHERE purpose_label = 'security_improvement'
	;`)
	if err != nil {
		return fmt.Errorf("setting purpose_id: %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_id = 4
		WHERE purpose_label = 'user_profiling'
	;`)
	if err != nil {
		return fmt.Errorf("setting purpose_id: %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET  purpose_id = 5
		WHERE purpose_label = 'marketing_communication'
	;`)
	if err != nil {
		return fmt.Errorf("setting purpose_id: %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_id = 6
		WHERE purpose_label = 'provision_to_partners'
	;`)
	if err != nil {
		return fmt.Errorf("setting purpose_id: %v", err)
	}

	_, err = tx.Exec(`UPDATE application_purpose
		SET purpose_id  =  7
		WHERE purpose_label = 'data_science'
	;`)
	if err != nil {
		return fmt.Errorf("setting purpose_id : %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE application_purpose
		ALTER COLUMN purpose_id SET NOT NULL
	;`)
	if err != nil {
		return fmt.Errorf("adding 'not null' constraint on purpose_id: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE application_purpose
		DROP purpose_label
	;`)
	if err != nil {
		return fmt.Errorf("dropping 'purpose_label` column: %v", err)
	}

	// Add contraint between purpose_label & application_id
	_, err = tx.Exec(`ALTER TABLE application_purpose
		ADD CONSTRAINT application_purpose_purpose_id_application_id_key UNIQUE (purpose_id,application_id)
	;`)
	if err != nil {
		return fmt.Errorf("fail to drop constraint application_purpose_purpose_id_application_id_key': %v", err)
	}

	// Add reference between purpose_id & purpose table
	_, err = tx.Exec(`ALTER TABLE application_purpose
		ADD CONSTRAINT application_purpose_purpose_id_key FOREIGN KEY (purpose_id) REFERENCES purpose (id) ON DELETE CASCADE MATCH FULL
	;`)
	if err != nil {
		return fmt.Errorf("add constraint foreign key application_purpose_purpose_id_key': %v", err)
	}

	return nil
}
