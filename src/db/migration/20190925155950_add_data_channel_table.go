package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(upAddDataChannelTable, downAddDataChannelTable)
}

func upAddDataChannelTable(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TYPE DATATYPE_LABEL_ENUM AS ENUM(
		'unclassified',
		'unclassified_purchase'
	);`)
	if err != nil {
		return fmt.Errorf("creating datatype label enum: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE datatype
		ADD label_tmp DATATYPE_LABEL_ENUM UNIQUE
	;`)
	if err != nil {
		return fmt.Errorf("adding column label_tmp in table datatype: %v", err)
	}

	_, err = tx.Exec(`UPDATE datatype
		SET label_tmp = 'unclassified'
		WHERE label = 'unclassified'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'unclassified': %v", err)
	}

	_, err = tx.Exec(`UPDATE datatype
		SET label_tmp = 'unclassified_purchase'
		WHERE label = 'unclassified_purchase'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'unclassified_purchase': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE datatype
		ADD CONSTRAINT datatype_label_key UNIQUE (label)
	;`)
	if err != nil {
		return fmt.Errorf("adding 'unique' constraint on datatype label: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE datatype
		ALTER COLUMN label_tmp SET NOT NULL
	;`)
	if err != nil {
		return fmt.Errorf("adding 'not null' constraint on datatype label: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE datatype
		DROP label
	;`)
	if err != nil {
		return fmt.Errorf("dropping 'label' column: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE datatype
		RENAME label_tmp TO label
	;`)
	if err != nil {
		return fmt.Errorf("renaming column to 'label': %v", err)
	}

	_, err = tx.Exec(`CREATE TABLE data_channel(
		id SERIAL PRIMARY KEY,
		owner_user_id UUID NOT NULL,
		datatype_label DATATYPE_LABEL_ENUM NOT NULL REFERENCES datatype(label),
		public_key VARCHAR(255) NOT NULL,
		created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
		UNIQUE (public_key),
		UNIQUE (owner_user_id, datatype_label)
	  );`)

	if err != nil {
		return fmt.Errorf("creating data_channel table: %v", err)
	}

	return err
}

func downAddDataChannelTable(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE data_channel`)
	if err != nil {
		return fmt.Errorf("dropping data_channel table: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE datatype
		RENAME label TO label_tmp
	;`)
	if err != nil {
		return fmt.Errorf("renaming column to 'label': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE datatype
		ADD label VARCHAR(255)
	;`)
	if err != nil {
		return fmt.Errorf("adding column label: %v", err)
	}

	_, err = tx.Exec(`UPDATE datatype
		SET label = 'unclassified_purchase'
		WHERE label_tmp = 'unclassified_purchase'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'unclassified_purchase': %v", err)
	}

	_, err = tx.Exec(`UPDATE datatype
		SET label = 'unclassified'
		WHERE label_tmp = 'unclassified'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'unclassified': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE datatype
		DROP label_tmp
	;`)
	if err != nil {
		return fmt.Errorf("dropping 'label_tmp' column: %v", err)
	}

	return err
}
