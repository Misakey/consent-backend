package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190729115320, Down20190729115320)
}

func Up20190729115320(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE datatype(
		id SERIAL PRIMARY KEY,
    label VARCHAR(255) NOT NULL,
    time_unit_seconds INTEGER NOT NULL
   );`)
	if err != nil {
		return fmt.Errorf("datatype table: %v", err)
	}

	// Insert default datatypes.
	// (2592000 time_unit_seconds == 1 month)
	_, err = tx.Exec(`
		INSERT INTO datatype (id, label, time_unit_seconds)
		VALUES
       (1, 'unclassified', 2592000),
       (2, 'unclassified_purchase', 2592000);
   `)

	return err
}

func Down20190729115320(tx *sql.Tx) error {
	_, err := tx.Exec(`
		DROP TABLE datatype;
   `)
	return err
}
