package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190814142714, Down20190814142714)
}

func Up20190814142714(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE user_consent(
		id SERIAL PRIMARY KEY,
		user_id UUID NOT NULL,
		created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
		application_purpose_id INTEGER NOT NULL REFERENCES application_purpose ON DELETE CASCADE,
		revoked_at timestamptz DEFAULT NULL,
		to_delete BOOL DEFAULT false
	   );`)

	return err
}

func Down20190814142714(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE user_consent`)
	return err
}
