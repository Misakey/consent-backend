package controller

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

// ConsentEcho provides function to bind to routes interacting with consent
type ConsentEcho struct {
	service consentService
}

// NewConsentEcho is ConsentEcho Controller constructor
func NewConsentEcho(service consentService) *ConsentEcho {
	return &ConsentEcho{
		service: service,
	}
}

// Delete is the handler for users/:id DELETE request
func (c *ConsentEcho) Delete(ctx echo.Context) error {
	id := ctx.Param("id")
	_, err := uuid.Parse(id)
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("id", merror.DVInvalid)
	}

	err = c.service.Delete(ctx.Request().Context(), id)
	if err != nil {
		return merror.Transform(err).Describe("could not delete user consents")
	}
	return ctx.NoContent(http.StatusNoContent)
}
