package controller

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/model"
)

// ConsentHydraEcho provides function to bind to routes interacting with consent
type ConsentHydraEcho struct {
	service hydraConsentService
}

// NewConsent is ConsentHydraEcho Controller constructor
func NewConsentHydraEcho(service hydraConsentService) *ConsentHydraEcho {
	return &ConsentHydraEcho{
		service: service,
	}
}

// Handles Hydra consent flow request
func (c *ConsentHydraEcho) InitConsent(ctx echo.Context) error {
	// parse parameters
	challenge := ctx.QueryParam("consent_challenge")

	redirectURL := c.service.InitUserConsents(ctx.Request().Context(), challenge)
	return ctx.Redirect(http.StatusFound, redirectURL)
}

// Handles user consent feedback about an app consent request
func (c *ConsentHydraEcho) Consent(ctx echo.Context) error {
	// check body parameters
	consent := model.UserDataConsents{}
	err := ctx.Bind(&consent)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&consent)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	acceptInfo, err := c.service.SetUserConsents(ctx.Request().Context(), consent)
	if err != nil {
		return merror.Transform(err).Describe("could not consent")
	}
	// do not redirect - frontend browser does not get this redirection
	return ctx.JSON(http.StatusSeeOther, acceptInfo)
}
