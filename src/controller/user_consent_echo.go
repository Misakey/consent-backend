package controller

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

type UserConsentEcho struct {
	service userConsentService
}

func NewUserConsentEcho(service userConsentService) *UserConsentEcho {
	return &UserConsentEcho{
		service: service,
	}
}

// List is the handler for user-consent GET request
func (uce *UserConsentEcho) List(ctx echo.Context) error {
	filters := model.UserConsentFilters{}
	filters.UserID = ctx.QueryParam("user_id")
	filters.ApplicationIDs = slices.FromSep(ctx.QueryParam("application_ids"), ",")

	err := ctx.Validate(&filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriQuery)
	}

	userConsents, err := uce.service.List(ctx.Request().Context(), filters)
	if err != nil {
		return merror.Transform(err).Describe("could not list user consents")
	}

	return ctx.JSON(http.StatusOK, userConsents)
}

// PartialUpdate is the handler for user-consent/:id PATCH request
func (uce *UserConsentEcho) PartialUpdate(ctx echo.Context) error {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("id", merror.DVInvalid)
	}

	// bind patched data to our model
	patchMap := patch.FromHTTP(ctx.Request()).ToModel(model.UserConsent{})
	if patchMap.Invalid {
		return merror.BadRequest().From(merror.OriBody).Describe("invalid patch entity")
	}

	userConsent := model.UserConsent{}
	userConsent.ID = id
	patchInfo := patchMap.GetInfo(&userConsent)
	err = ctx.Bind(&userConsent)
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Describe(err.Error())
	}

	err = uce.service.PartialUpdate(ctx.Request().Context(), &patchInfo)
	if err != nil {
		return merror.Transform(err).Describe("could not revoke user consent")
	}
	return ctx.NoContent(http.StatusNoContent)
}
