package controller

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

type ApplicationPurposeDatatypeEcho struct {
	service applicationPurposeDatatypeService
}

// NewApplicationPurposeDatatypeEcho constructor
func NewApplicationPurposeDatatypeEcho(service applicationPurposeDatatypeService) *ApplicationPurposeDatatypeEcho {
	return &ApplicationPurposeDatatypeEcho{
		service: service,
	}
}

// Create is the handler for application-purpose-datatype POST request
func (apde *ApplicationPurposeDatatypeEcho) Create(ctx echo.Context) error {
	appPurposeDatatype := model.ApplicationPurposeDatatype{}

	err := ctx.Bind(&appPurposeDatatype)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&appPurposeDatatype)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = apde.service.Create(ctx.Request().Context(), &appPurposeDatatype)
	if err != nil {
		return merror.Transform(err).Describe("could not create application purpose datatype")
	}

	return ctx.JSON(http.StatusCreated, appPurposeDatatype)
}

// List is the handler for application-purpose-datatype GET request
func (apde *ApplicationPurposeDatatypeEcho) List(ctx echo.Context) error {
	filters := model.ApplicationPurposeDatatypeFilters{}
	filters.IDs = slices.FromSep(ctx.QueryParam("ids"), ",")
	if len(filters.IDs) == 0 {
		return merror.BadRequest().From(merror.OriQuery).Detail("ids", merror.DVRequired)
	}

	appPurposesDatatype, err := apde.service.List(ctx.Request().Context(), filters)
	if err != nil {
		return merror.Transform(err).Describe("could not list application purpose datatype")
	}

	return ctx.JSON(http.StatusOK, appPurposesDatatype)
}

// Delete is the handler for application-purpose-datatype DELETE request
func (apde *ApplicationPurposeDatatypeEcho) Delete(ctx echo.Context) error {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("id", merror.DVInvalid)
	}

	err = apde.service.Delete(ctx.Request().Context(), id)
	if err != nil {
		return merror.Transform(err).Describe("could not delete application purpose datatype")
	}
	return ctx.NoContent(http.StatusNoContent)
}
