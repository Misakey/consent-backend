package controller

import (
	"net/http"
	"os"

	"github.com/labstack/echo"

	"gitlab.com/Misakey/consent-backend/src/model"
)

// Generic information about the API
type Generic struct{}

// Generic constructor
func NewGeneric() *Generic {
	return &Generic{}
}

// Version
func (g *Generic) Version(ctx echo.Context) error {
	// get version from environment variable
	// if unset, will set an empty value
	api := model.API{
		Version: os.Getenv("VERSION"),
	}

	return ctx.JSON(http.StatusOK, api)
}
