package controller

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/adaptor/convert"
	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

type CryptogramEcho struct {
	service cryptogramService
}

// NewCryptogramEcho constructor
func NewCryptogramEcho(service cryptogramService) *CryptogramEcho {
	return &CryptogramEcho{
		service: service,
	}
}

// Create is the handler for cryptogram POST request
func (c *CryptogramEcho) Create(ctx echo.Context) error {
	cryptogram := model.Cryptogram{}

	err := ctx.Bind(&cryptogram)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&cryptogram)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = c.service.Create(ctx.Request().Context(), &cryptogram)
	if err != nil {
		return merror.Transform(err).Describe("could not create cryptogram")
	}

	return ctx.JSON(http.StatusCreated, cryptogram)
}

// List is the handle for cryptogram GET request
func (c *CryptogramEcho) List(ctx echo.Context) error {
	filters := model.CryptogramlFilters{}
	filters.OwnerUserID = ctx.QueryParam("owner_user_id")
	filters.DatatypeLabels = slices.FromSep(ctx.QueryParam("datatype_labels"), ",")
	filters.PurposeLabels = slices.SpecialFromSep(ctx.QueryParam("purpose_labels"), ",")
	filters.ProducerApplicationID = ctx.QueryParam("producer_application_id")
	toDatetime, err := convert.ToTime(ctx.QueryParam("to_datetime"))
	if err != nil {
		return merror.BadRequest().
			Describe(err.Error()).
			From(merror.OriQuery).
			Detail("to_datetime", merror.DVInvalid)
	}
	if toDatetime != nil {
		filters.ToDatetime = *toDatetime
	}

	fromDatetime, err := convert.ToTime(ctx.QueryParam("from_datetime"))
	if err != nil {
		return merror.BadRequest().
			Describe(err.Error()).
			From(merror.OriQuery).
			Detail("from_datetime", merror.DVInvalid)
	}
	if fromDatetime != nil {
		filters.FromDatetime = *fromDatetime
	}

	filters.Limit, err = convert.ToOptionalInt64(ctx.QueryParam("limit"))
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("limit", merror.DVInvalid)
	}
	filters.Offset, err = convert.ToOptionalInt64(ctx.QueryParam("offset"))
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("offset", merror.DVInvalid)
	}

	err = ctx.Validate(&filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriQuery)
	}

	retrievedDataChannels, err := c.service.List(ctx.Request().Context(), filters)
	if err != nil {
		return merror.Transform(err).Describe("could not list data channels")
	}

	return ctx.JSON(http.StatusOK, retrievedDataChannels)
}
