package controller

import (
	"context"

	"gitlab.com/Misakey/consent-backend/src/model"
	"gitlab.com/Misakey/msk-sdk-go/patch"
)

type purposeService interface {
	List(ctx context.Context) ([]*model.Purpose, error)
}

type applicationPurposeService interface {
	Create(ctx context.Context, appPurpose *model.ApplicationPurpose) error
	List(ctx context.Context, filters model.ApplicationPurposeFilters) ([]*model.ApplicationPurpose, error)
	Delete(ctx context.Context, id int) error
}

type hydraConsentService interface {
	InitUserConsents(ctx context.Context, challenge string) string
	SetUserConsents(ctx context.Context, consents model.UserDataConsents) (*model.ConsentAcceptInfo, error)
}

type consentService interface {
	Delete(ctx context.Context, id string) error
}

type applicationPurposeDatatypeService interface {
	Create(ctx context.Context, appPurposeDatatype *model.ApplicationPurposeDatatype) error
	List(ctx context.Context, filters model.ApplicationPurposeDatatypeFilters) ([]*model.ApplicationPurposeDatatype, error)
	Delete(ctx context.Context, id int) error
}

type userConsentService interface {
	List(ctx context.Context, filters model.UserConsentFilters) ([]*model.UserConsent, error)
	PartialUpdate(ctx context.Context, pi *patch.Info) error
}

type dataChannelService interface {
	Create(ctx context.Context, data_channel []*model.DataChannel) error
	List(ctx context.Context, filters model.DataChannelFilters) ([]*model.DataChannel, error)
}

type cryptogramService interface {
	Create(ctx context.Context, cryptogram *model.Cryptogram) error
	List(ctx context.Context, filters model.CryptogramlFilters) ([]*model.Cryptogram, error)
}
