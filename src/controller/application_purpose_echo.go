package controller

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

type ApplicationPurposeEcho struct {
	service applicationPurposeService
}

// NewApplicationPurposeEcho constructor
func NewApplicationPurposeEcho(service applicationPurposeService) *ApplicationPurposeEcho {
	return &ApplicationPurposeEcho{
		service: service,
	}
}

// Create is the handler for application-purpose POST request
func (ape *ApplicationPurposeEcho) Create(ctx echo.Context) error {
	appPurpose := model.ApplicationPurpose{}

	err := ctx.Bind(&appPurpose)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&appPurpose)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = ape.service.Create(ctx.Request().Context(), &appPurpose)
	if err != nil {
		return merror.Transform(err).Describe("could not create application purpose")
	}

	return ctx.JSON(http.StatusCreated, appPurpose)
}

// List is the handler for application-purpose GET request
func (ape *ApplicationPurposeEcho) List(ctx echo.Context) error {
	var err error
	filters := model.ApplicationPurposeFilters{}
	filters.IDs, err = slices.SplitInt(ctx.QueryParam("ids"), ",")
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).
			Describe(err.Error()).Detail("ids", merror.DVInvalid)
	}
	filters.AppIDs = slices.SpecialFromSep(ctx.QueryParam("application_ids"), ",")
	filters.PurposeLabels = slices.SpecialFromSep(ctx.QueryParam("purpose_labels"), ",")

	err = ctx.Validate(&filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriQuery)
	}

	appPurposes, err := ape.service.List(ctx.Request().Context(), filters)
	if err != nil {
		return merror.Transform(err).Describe("could not list application purpose")
	}

	return ctx.JSON(http.StatusOK, appPurposes)
}

// Delete is the handler for application-purpose DELETE request
func (ape *ApplicationPurposeEcho) Delete(ctx echo.Context) error {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("id", merror.DVInvalid)
	}

	err = ape.service.Delete(ctx.Request().Context(), id)
	if err != nil {
		return merror.Transform(err).Describe("could not delete application purpose")
	}
	return ctx.NoContent(http.StatusNoContent)
}
