package controller

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/consent-backend/src/adaptor/slices"
	"gitlab.com/Misakey/consent-backend/src/model"
)

type DataChannelEcho struct {
	service dataChannelService
}

func NewDataChannelEcho(service dataChannelService) *DataChannelEcho {
	return &DataChannelEcho{
		service: service,
	}
}

func (ctrl *DataChannelEcho) Create(ctx echo.Context) error {
	dataChannels := []*model.DataChannel{}

	err := ctx.Bind(&dataChannels)
	if err != nil {
		return merror.BadRequest().Describe(err.Error()).From(merror.OriBody)
	}

	for _, dataChannel := range dataChannels {
		err = ctx.Validate(dataChannel)
		if err != nil {
			return merror.Transform(err).From(merror.OriBody)
		}
	}

	err = ctrl.service.Create(ctx.Request().Context(), dataChannels)
	if err != nil {
		return merror.Transform(err).Describe("could not create")
	}
	return ctx.JSON(http.StatusCreated, dataChannels)
}

func (ctrl *DataChannelEcho) List(ctx echo.Context) error {
	filters := model.DataChannelFilters{}
	filters.OwnerUserID = ctx.QueryParam("owner_user_id")
	filters.DatatypeLabels = slices.FromSep(ctx.QueryParam("datatypes"), ",")

	err := ctx.Validate(&filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriQuery)
	}

	retrievedDataChannels, err := ctrl.service.List(ctx.Request().Context(), filters)
	if err != nil {
		return merror.Transform(err).Describe("could not list data channels")
	}

	return ctx.JSON(http.StatusOK, retrievedDataChannels)
}
