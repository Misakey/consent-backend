package controller

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

type PurposeEcho struct {
	service purposeService
}

func NewPurposeEcho(service purposeService) *PurposeEcho {
	return &PurposeEcho{
		service: service,
	}
}

func (pe PurposeEcho) List(ctx echo.Context) error {
	purposes, err := pe.service.List(ctx.Request().Context())
	if err != nil {
		return merror.Transform(err).Describe("could not list purposes")
	}

	return ctx.JSON(http.StatusOK, purposes)
}
