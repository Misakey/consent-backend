package slices

import (
	"strconv"
	"strings"
)

// ContainsString return true if the candidate string is contained inside container,
// container being a slice of string
func ContainsString(container []string, candidate string) bool {
	for _, element := range container {
		if element == candidate {
			return true
		}
	}
	return false
}

// EscapedStrings take a string slices and return it as a string
// with quote around elems of the list, separated by commas
// The purpose of this helper is to facilitate WHERE IN query building
func EscapedStrings(strSlice []string) string {
	for pos, elem := range strSlice {
		strSlice[pos] = "'" + elem + "'"
	}
	return strings.Join(strSlice, ",")
}

// EscapedStringsCopy take a string slices and return it as a string
// with quote around elems of the list, separated by commas
// The purpose of this helper is to facilitate WHERE IN query building
// This method does not directly modify the input array as the original one
func EscapedStringsCopy(strSlice []string) string {
	copy := make([]string, len(strSlice))
	for pos, elem := range strSlice {
		copy[pos] = "'" + elem + "'"
	}
	return strings.Join(copy, ",")
}

// FromSep use strings.Split to split a {sep}-separated list string into a slice
// It handles the fact strings.Split return a slice of size 1 containing empty string if the sepStr is empty
// We return then, an empty slice instead of this default strings.Split behavior
func FromSep(sepStr string, sep string) []string {
	if len(sepStr) == 0 {
		return []string{}
	}
	return strings.Split(sepStr, sep)
}

// SpecialFromSep use strings.Split to split a {sep}-separated list string into a slice
// It handles the fact strings.Split return a slice of size 1 containing empty string if the sepStr is empty
// We return then, a nul instead of this default strings.Split behavior
func SpecialFromSep(sepStr string, sep string) []string {
	if len(sepStr) == 0 {
		return nil
	}
	return strings.Split(sepStr, sep)
}

// SplitInt use strings.Split to split a {sep}-separated list string into a slice
// Then it converts all strings into integer and return an slice of interger
// It returns a nil value if input string is empty, and error in case of conversion error
func SplitInt(sepStr string, sep string) ([]int, error) {
	if len(sepStr) == 0 {
		return nil, nil
	}
	var ints []int
	strs := strings.Split(sepStr, sep)
	for _, val := range strs {
		intVal, err := strconv.Atoi(val)
		if err != nil {
			return nil, err
		}
		ints = append(ints, intVal)
	}
	return ints, nil
}

// JoinInt use strings.Join to join a slice of integer into a {sep}-separated list string
// it converts all integer into strings and return an slice of strings
// It returns a nil value if input string is empty
func JoinInt(ints []int, sep string) string {
	if len(ints) == 0 {
		return ""
	}
	strs := make([]string, len(ints))
	for i, val := range ints {
		strVal := strconv.Itoa(val)
		strs[i] = strVal
	}
	return strings.Join(strs, sep)
}

// RemoveString remove a particular string from another string
// where source is a list of strings, space separated
// element are not remove from source
// Always return a new string
func RemoveString(source string, toRemove string) string {
	if toRemove == "" {
		return source
	}

	sourceSlice := FromSep(source, " ")
	for i, element := range sourceSlice {
		if element == toRemove {
			sourceSlice = append(sourceSlice[:i], sourceSlice[i+1:]...)
		}
	}

	return strings.Join(sourceSlice, " ")
}

// RemoveStringFromArray remove a particular string from a slice
// where source is an array of string
// element is remove from source
// return the updated slice
func RemoveStringFromArray(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

// RemoveIntFromArray remove a particular int from a slice
// where source is an array of int
// element is remove from source
// return the updated slice
func RemoveIntFromArray(s []int, r int) []int {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

func StringSliceToInterfaceSlice(x []string) []interface{} {
	result := make([]interface{}, len(x))
	for index, elt := range x {
		result[index] = elt
	}
	return result
}

func IntSliceToInterfaceSlice(x []int) []interface{} {
	result := make([]interface{}, len(x))
	for index, elt := range x {
		result[index] = elt
	}
	return result
}
