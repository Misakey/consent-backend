package convert

import (
	"strconv"
	"time"
)

// ToTime convert string to time.Time (RFC3339)
func ToTime(queryParam string) (*time.Time, error) {
	if len(queryParam) == 0 {
		return nil, nil
	}
	date, err := time.Parse(time.RFC3339, queryParam)
	if err != nil {
		return nil, err
	}
	return &date, nil
}

// ToOptionalInt64 convert string to int64
func ToOptionalInt64(queryParam string) (*int64, error) {
	if len(queryParam) == 0 {
		return nil, nil
	}
	n, err := strconv.ParseInt(queryParam, 10, 64)
	if err != nil {
		return nil, err
	}
	return &n, nil
}
