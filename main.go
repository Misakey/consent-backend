package main

import (
	"fmt"
	"os"

	"gitlab.com/Misakey/consent-backend/src/cmd"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
