# ----------------------------
#       CONFIGURATION
# ----------------------------

# Import deploy config
dpl ?= deploy.env
include $(dpl)
export $(shell sed 's/=.*//' $(dpl))

# This version-strategy uses git tags to set the version string
VERSION := $(shell git describe --tags --always --dirty)

# Set gitlab-ci variables if not in a CI context
ifndef CI_REGISTRY_IMAGE
	CI_REGISTRY_IMAGE := $(DOCKER_REGISTRY)/misakey/$(APP_NAME)
endif
ifndef CI_COMMIT_REF_NAME
	CI_COMMIT_REF_NAME := $(shell git rev-parse --abbrev-ref HEAD)
endif
CI_COMMIT_REF_NAME := $(shell if echo "$(CI_COMMIT_REF_NAME)" | grep -q "/"; then echo $(CI_COMMIT_REF_NAME) |  sed -n "s/^.*\/\(.*\)$$/\1/p"; else echo $(CI_COMMIT_REF_NAME); fi)

# Set default goal (`make` without command)
.DEFAULT_GOAL := help

# ----------------------------
#          COMMANDS
# ----------------------------

.PHONY: echo
echo:
	@echo "$(CI_COMMIT_REF_NAME)"

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.PHONY: dep
dep: ## Install all dependencies in the vendor folder
	@GO111MODULE=on go mod download
	@GO111MODULE=on go mod vendor

.PHONY: docs
docs: ## Validate documentation
ifeq (, $(shell which openapi-spec-validator))
	@echo "Installing openapi-spec-validator..."
	@pip install openapi-spec-validator
endif
	@openapi-spec-validator ./docs/swagger/index.yaml

.PHONY: deploy-docs
deploy-docs: ## Deploy documentation. USE ONLY ON PRODUCTION!
ifeq (, $(shell which aws))
	@echo "Installing awscli..."
	@pip install awscli
endif
	@aws s3 rm s3://misakey-internal-use/swagger/consent/ --recursive --exclude "*" --include "*.yaml"
	@aws s3 cp ./docs/swagger/ s3://misakey-internal-use/swagger/consent/ --recursive --exclude "*" --include "*.yaml"


.PHONY: test
test: ## Unit test code
	@GO111MODULE=on go test -short -mod=vendor ./...

.PHONY: lint
lint: ## Lint project code with golint
ifeq (, $(shell which golint))
	@GO111MODULE=off go get -u golang.org/x/lint/golint
endif
	golint ./src/...

.PHONY: helm-lint
helm-lint: ## Lint Helm chart
	@helm lint ./helm/consent-backend --set env=production --set image.tag=latest

.PHONY: strict-lint
strict-lint: ## Lint project code with golint, return error if there is any suggestion
ifeq (, $(shell which golint))
	@GO111MODULE=off go get -u golang.org/x/lint/golint
endif
	golint -set_exit_status ./src/...

.PHONY: docker-login
docker-login: ## Log in to the default registry
	@docker login -u $(CI_REGISTRY_USER) -p $(CI_REGISTRY_PASSWORD) $(DOCKER_REGISTRY)

.PHONY: build
build: ## Build a docker image with the Go binary
	@docker build --build-arg VERSION=$(VERSION) -t $(CI_REGISTRY_IMAGE):$(VERSION) .

.PHONY: tag
tag: ## Tag a docker image and set some aliases
ifeq ($(CI_COMMIT_REF_NAME),master)
	@docker tag $(CI_REGISTRY_IMAGE):$(VERSION) $(CI_REGISTRY_IMAGE):latest
endif
ifeq ($(CI_COMMIT_REF_NAME),release)
	@docker tag $(CI_REGISTRY_IMAGE):$(VERSION) $(CI_REGISTRY_IMAGE):rc
endif
	@docker tag $(CI_REGISTRY_IMAGE):$(VERSION) $(CI_REGISTRY_IMAGE):$(CI_COMMIT_REF_NAME)

.PHONY: deploy
deploy: ## Push image to the docker registry
	@docker push $(CI_REGISTRY_IMAGE):$(VERSION)
ifeq ($(CI_COMMIT_REF_NAME),master)
	@docker push $(CI_REGISTRY_IMAGE):latest
endif
ifeq ($(CI_COMMIT_REF_NAME),release)
	@docker push $(CI_REGISTRY_IMAGE):rc
endif
	@docker push $(CI_REGISTRY_IMAGE):$(CI_COMMIT_REF_NAME)

.PHONY: clean
clean: ## Remove all images related to the project
	@docker images | grep $(CI_REGISTRY_IMAGE) | tr -s ' ' | cut -d ' ' -f 2 | xargs -I {} docker rmi $(CI_REGISTRY_IMAGE):{}
